@echo off
::build keycloak extension jar
echo -----------------------
echo build keycloak extension jar
echo -----------------------
call cd ./keycloak-extension/
call ./gradlew jar
call cd ../
::start container
echo -----------------------
echo start docker container
echo -----------------------
docker-compose up
