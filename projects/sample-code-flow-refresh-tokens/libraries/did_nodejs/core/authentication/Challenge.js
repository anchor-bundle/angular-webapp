/*
	Implements a Challenge for the challenge-response-cycle!
*/

//> Dependencies!
import * as _cryptographyWrapper from "./../wrappers/cryptographyWrapper.js";
import {Timestamp as _Timestamp} from "./../ssi/Timestamp.js";

class Challenge {

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Construction  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Constructs a new Challenge object!
	constructor (
		id = undefined,				//> Unique ID to be used to identify the Challenge (esp. on server-side)!
		timestamp = undefined,		//> Timestamp of creation (mostly used for valid-until)!
		randomString = undefined,	//> Actual challenge, randomized!
		signature = undefined,		//> Signature!
		randomStringBytes = 32		//> Bytes of challenge if random instead of provided!
		) {

		this._id = id || _cryptographyWrapper.rstr(8);
		this._timestamp = timestamp || new _Timestamp(timestamp);
		this._randomString = randomString || _cryptographyWrapper.rstr(randomStringBytes);
		this._signature = signature;

	};

	//> Constructs from JSON!
	static fromJSON(j) {

		const id = j.id;
		const timestamp = new _Timestamp(j.timestamp);
		const randomString = j.randomString;
		const signature = j.signature;
		return new Challenge(id, timestamp, randomString, signature);

	};

	//> Constructs from string!
	static fromString(str) {

		return Challenge.fromJSON(JSON.parse(str));

	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Getters & Setters  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Getters!
	get id(){
		return this._id;
	};
	get timestamp(){
		return this._timestamp;
	};
	get randomString(){
		return this._randomString;
	};
	get signature(){
		return this._signature;
	};

	//> Setters!
	set id(v){
		this._id = v;
	};
	set timestamp(v){
		this._timestamp = v;
	};
	set randomString(v){
		this._randomString = v;
	};
	set signature(v){
		this._signature = v;
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Conversion  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Converts to JSON!
	toJSON(noSignature = false) {

		var j = {
			"id": this._id,
			"timestamp": this._timestamp.toJSON(), //> Follows https://www.w3.org/TR/NOTE-datetime and ISO 8601!
			"randomString": this._randomString
		};
		if(noSignature == false){
			j.signature = this._signature;
		};
		return j;

	};

	//> Converts to string!
	toString() {
		return JSON.stringify(this.toJSON());
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Cryptography  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Signs this Challenge using a private key either as string or as crypto object!
	sign(privateKey){

		//> Check if signature already exists!
		if(this._signature !== undefined){
			return undefined;
		};

		//> Convert the private key string to object first, if neccessary!
		if(typeof(privateKey) == "string"){
			privateKey = _cryptographyWrapper.stringToPrivateKey(privateKey);
		};

		//> Construct Proof!
		const data = this.toJSON(true);
		this._signature = _cryptographyWrapper.sign(data, privateKey);

		//> Return itself for good measure!
		return this;

	};

	//> Verifies this Challenge's signature!
	verify(publicKey){ //> Note: Resolving towards the publicKey is out of scope and should be done on the server directly!

		//> Check if signature exists!
		if(this._signature == undefined){
			return false;
		};

		//> Convert the public key string to object first, if neccessary!
		if(typeof(publicKey) == "string"){
			publicKey = _cryptographyWrapper.stringToPublicKey(publicKey);
		};

		//> Verify and return!
		const data = this.toJSON(true);
		return _cryptographyWrapper.verify(data, publicKey, this._signature);

	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Export  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

}; //> END class Challenge!

//> List of module exports!
export {
	Challenge,
};