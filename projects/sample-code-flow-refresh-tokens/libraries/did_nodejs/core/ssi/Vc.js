/*
	Implements Verifiable Credentials (VCs)!
*/

//> Dependencies!
import { base64 } from "ethers/lib/utils.js";
import * as _errors from "./../err/c_err.js";
//> Schema dependencies!
import { schema as _VerifiableCredentialSchema } from "./../schema/schema_VerifiableCredential.js";
import * as _cryptographyWrapper from "./../wrappers/cryptographyWrapper.js";
import { Context as _Context } from "./Context.js";
import { Did as _Did } from "./Did.js";
import { Proof as _Proof } from "./Proof.js";
import { PublicKeyReference as _PublicKeyReference } from "./PublicKeyReference.js";
import { Timestamp as _Timestamp } from "./Timestamp.js";
import { Uri as _Uri } from "./Uri.js";


class Vc {

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Construction  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//> Creates a new VC from scratch!
	constructor (
		issuerDIDOBJ, //> The Did Object of the entity that issued the credential!
		subjectDIDOBJ, //> The Did Object of the entity that the credential was issued for!
		claims, //> List of {claimName:claimName, claimContent:claimContent} objects detailing claims about the subject!
		context = "https://www.w3.org/2018/credentials/v1", //> The (list of) context(s) for this VC!
		id = "http://example.com/credentials/" + Math.random().toString(4).substring(2, 6), //> The VC identifier - can be anything, really. Defaults to a random URI string!
		type = ["VerifiableCredential"], //> List of credential types declaring expected data within credential!
		issuanceDateString = undefined, //> The date of issuance for the credential, as a conforming string or undefined for "now"!
		proof = undefined, //> The complete Proof object, in case we construct from JSON!
		){
		
		//> Fill in data!
		this._data = {};
		this._data.context = new _Context(context); //> @context attribute!
		this._data.id = new _Uri(id);
		this._data.type = type;
		this._data.issuer = issuerDIDOBJ;
		this._data.issuanceDate = new _Timestamp(issuanceDateString);
		this._data.proof = proof;

		//> Fill in claims about subject!
		this._data.credentialSubject = {};
		this._data.credentialSubject.id = subjectDIDOBJ;
		this._data.credentialSubject.claims = claims;
	};

	//> Creates a Verifiable Credential (object) from JSON!
	static fromJSON(j){
		//check if json is complitly
		if(j && j.credentialSubject && j.issuer && j.credentialSubject.id && j["@context"] && j.id && j.type && j.issuanceDate){
		//> Extract claims!
		var claimsList = []
		for (var key in j.credentialSubject){
			if(key != "id"){
				claimsList.push({"claimName": key, "claimContent": j.credentialSubject[key]});
			};
		};

		//> Check for proof!
		var proof = undefined;
		
		if(j.proof != undefined){
			//extrakt signature from the jws
			const regex = /.*(\.\.)/;
			let signature =base64.decode(j.proof.jws.replace(regex,'')); 			
			proof = new _Proof(signature, _PublicKeyReference.fromJSON(j.proof.verificationMethod), j.proof.created, j.proof.type, j.proof.proofPurpose);
		};		

		//> Create new Verifiable Credential object!
		return new Vc(
			new _Did(j.issuer),
			new _Did(j.credentialSubject.id),
			claimsList,
			j["@context"],
			j.id,
			j.type,
			j.issuanceDate,
			proof
		);
		} else {
			throw new Error("input Vc is not containing all needed properties");
		}
	};

	//> Returns a stringify-able JSON representation of itself!
	toJSON(ignoreFields = []){
		var json = {};
		for (var key in this._data) {
			if (!(ignoreFields.includes(key))) {
				switch (key) {
					case "issuer":
					json[key] = this._data[key];
					break;
					case "id":
					case "issuanceDate":
					case "proof":
						if(this._data[key] != undefined){
							json[key] = this._data[key].toJSON();
						};
						break;
					case "context":
						json["@context"] = this._data[key].toJSON(true); //> Force list for context attribute, since this is a VC!
						break;
					case "credentialSubject": //> Iterate through claims!
						var j = {};
						j.id = this._data[key].id.toJSON();
						for (var claimObj of this._data[key].claims){
							j[claimObj["claimName"]] = claimObj["claimContent"];
						};
						json[key] = j;
						break;
					default:
						json[key] = this._data[key];
						break;
				}
			}
		}
		return json;
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Getters, Setters  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Getters!
	get context() {
		return this._data.context;
	};
	get id() {
		return this._data.id;
	};
	get type() {
		return this._data.type;
	};
	get issuer() {
		return this._data.issuer;
	};
	get issuanceDate() {
		return this._data.issuanceDate;
	};
	get proof() {
		return this._data.proof;
	};
	get subjectId() {
		return this._data.credentialSubject.id;
	};
	get claims() {
		return this._data.credentialSubject.claims;
	};
	
	//> Setters!
	set context(v) {
		this._data.context = v;
	};
	set id(v) {
		this._data.id = v;
	};
	set type(v) {
		this._data.type = v;
	};
	set issuer(v) {
		this._data.issuer = v;
	};
	set issuanceDate(v) {
		this._data.issuanceDate = v;
	};
	set proof(v) {
		this._data.proof = v;
	};
	set subjectId(v) {
		this._data.credentialSubject.id = v;
	};
	set claims(v) {
		this._data.credentialSubject.claims = v;
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Validation and verification  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Returns a boolean indicating cryptographic validity of the VC!
	verify(didDocumentObject){
		return Vc.verify(this, didDocumentObject);
	};

	//> Returns a boolean indicating cryptographic validity of the VC!
	static verify(verifiableCredentialObject, didDocumentObject){

		/*
			A note on VC verification:
			We need a DD for this since the VC itself only stores a PKR - not the actual PKO, and neither the actual _key_.
			This means we have to resolve the PKR into a PKO and retreive the key from the PKO in order to verify the proof within the VC.
			Hence, we have to pass the DD that was used to sign the VC as a parameter here. In reality/practise, we verifier would then receive a DID _only_, and retrieve the DD from the DLT.
		*/

		//> Check if proof exists!
		if(verifiableCredentialObject.proof == undefined){
			return false;
		};

		//> Check if proof doesn't contain syntactic errors!
		try{

			//> Resolve the VC's PKR to a PKO using the DD!
			var publicKeyObject = verifiableCredentialObject.proof.creator.resolve(didDocumentObject).publicKey;

			//> Initialize some variables!
			const signature = verifiableCredentialObject.proof.signatureValue;
			const vcSinProof = verifiableCredentialObject.toJSON(["proof"]);

			//> Verify!
			return _cryptographyWrapper.verify(vcSinProof, publicKeyObject, signature);

		} catch { //> Return false if anything is invalid!
			return false;
		}
		throw _errors.VC_DRAGONS;

	};

	//> Syntactically validates the Verifiable Credential!
	validate(){
		return Vc.validate(this.toJSON(), _VerifiableCredentialSchema);
	};



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Auxiliary  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	

	//> Add cryptographic proof to VC!
	addProof(privateKey, publicKeyReferenceIdentifier = "key-1") {

		//> Check if there is a proof already!
		if(this._data.proof !== undefined){
			throw _errors.VC_PROOF_ERROR;
		};

		const creatorPKR = new _PublicKeyReference(this._data.issuer, publicKeyReferenceIdentifier);
		const signatureValue = _cryptographyWrapper.sign(this.toJSON(["proof"]), privateKey);
		this._data.proof = new _Proof(signatureValue, creatorPKR, undefined, undefined, "assertionMethod");

	};

};

//> List of module exports!
export {
	Vc,
};
