/*
	Implements a reference to a PublicKey object!
*/

//> Dependencies!
import {Did as Did} from "./Did.js";

class PublicKeyReference {
	
	//> Constructs a new PublicKey object!
	constructor (did, suf, sep = "#") {

		this._did = did; //> DID of PublicKey Object that is referred to!
		this._suf = suf; //> Suffix, e.g. "key-1"!
		this._sep = sep; //> Seperator, defaults to "#"!

	};

	//> Constructs from string!
	static fromString(s, sep = "#"){ //> From e.g. did:iota:main:AZHGSDJ...JHAGSDJH#key-1 !
		var spl = s.split(sep);
		return new PublicKeyReference(Did.fromString(spl[0]), spl[1], sep);
	};

	//> Constructs from JSON!
	static fromJSON(j, sep = "#") {
		return PublicKeyReference.fromString(j, sep);
	};
	
	//> Getters!
	get did() {
		return this._did;
	};
	get suffix() {
		return this._suf;
	};
	get seperator() {
		return this._sep;
	};

	//> Setters!
	set did(v) {
		this._did = v;
	};
	set suffix(v) {
		this._suf = v;
	};
	set seperator(v) {
		this._sep = v;
	};
	
	//> Returns a string representation of itself!
	toString(){
		return this._did.toString() + this._sep + this._suf;
	};

	//> Returns a JSON representation of itself!
	toJSON(){
		return this.toString();
	};

	//> Resolves this PublicKeyReference into a PublicKey object given a DID Document that contains the referenced PublicKey object. This is a prerequisite for e.g. verifying VCs!
	resolve(DidDocumentObj){
		for(var pko of DidDocumentObj.publicKey){
			if(pko.id.toString() === this.toString()){ //> PublicKeyReferences must be the same for this PKR to resolve to the currently iterated one!
				return pko;
			};
		};
		return undefined;
	};
	
};

//> List of module exports!
export {
	PublicKeyReference,
};