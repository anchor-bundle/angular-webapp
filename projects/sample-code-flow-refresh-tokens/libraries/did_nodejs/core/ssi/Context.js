/*
	Implements @context attribute as defined in e.g. https://w3c.github.io/vc-data-model/#dfn-context and https://www.w3.org/TR/did-core/#example-12-an-example-of-a-relative-did-url !
*/

class Context {

	//> Creates a new Context object!
	constructor (context = ["https://www.w3.org/ns/did/v1"]) {

		//> Context can either be a string (one context) or a list!
		if (typeof(context) == "string"){ //> May be a string or a list in DID Documents, but only a list on VCs, independently of number of elements!
			this._context = [context]; //> E.g. "https://www.w3.org/2018/credentials/v1" or "https://www.w3.org/ns/did/v1" !
		} else {
			this._context = context;
		}
		
	};
	
	//> Creates a new Context object from JSON!
	static fromJSON(j){
		return new Context(j);
	};

	//> Getters!
	get context() {
		return this._context;
	};
	
	//> Setters!
	set context(v) {
		this._context = v;
	};

	//> Returns JSON representation of Context object!
	toJSON(forceList = true){

		if(this._context.length == 1){
			if(forceList == true){
				return this._context; //> For VCs!
			}
			return this._context[0]; //> For DID Documents!
		}
		return this._context;
	};
	
};

//> List of module exports!
export {
	Context,
};