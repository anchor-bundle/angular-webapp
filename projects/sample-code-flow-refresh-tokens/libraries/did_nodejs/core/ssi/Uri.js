/*	
	Implements a URI !
*/

//> Implements a URI!
class Uri {
	
	//> Constructs a new DID!
	constructor (uri = "http://example.com/credentials/1337") {

		this._uri = uri;

	};

	//> Construct from string!
	static fromString(s){
		return new Uri(s);
	};

	//> Construct from JSON!
	static fromJSON(j){
		return Uri.fromString(j)
	};
	
	//> Getters!
	get uri() {
		return this._uri;
	};
	
	//> Setters!
	set uri(v) {
		this._uri = v;
	};

	//> Creates a string representation out of the URI!
	toString() {
		return this._uri;
	};

	//> Creates a JSON representation out of the URI!
	toJSON(){
		return this.toString();
	};

};

//> List of module exports!
export {
	Uri,
};