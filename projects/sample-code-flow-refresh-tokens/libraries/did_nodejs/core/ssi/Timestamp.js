/*
	Implements Timestamp (wrapper) following https://www.w3.org/TR/NOTE-datetime (and ISO 8601), https://www.w3.org/TR/xmlschema11-2/#d-t-values and rfc3339!
*/

class Timestamp {

	//> Creates a new Timestamp object!
	constructor (timestamp = undefined) {
		this._timestamp = new Date(timestamp);
		if(this.isValid() == false){
			this._timestamp = new Date(); //> Timestamp == now iff not provided with a valid timestamp!
		};
	};

	//> Creates a new Timestamp object from a string!
	static fromString(s){
		return new Timestamp(s);
	};

	//> Creates a new Timestamp object from JSON!
	static fromJSON(j){
		return this.fromString(j);
	};

	//> Getters!
	get timestamp() {
		return this._timestamp;
	};
	
	//> Setters!
	set timestamp(v) {
		this._timestamp = v;
	};

	//> Returns a boolean indicating validity of the timestamp!
	isValid(){
		return this._timestamp instanceof Date && !isNaN(this._timestamp);
	};

	//> Converts the timestamp to a miliseconds from 1970 string!
	toString(){
		return this._timestamp.valueOf();
	};

	//> Converts the timestamp to JSON!
	toJSON(){
		return this.toString();
	};
	
};

//> List of module exports!
export {
	Timestamp,
};
