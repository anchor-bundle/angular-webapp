/*	
	Implements a Decentralized Identitifer (DID) following https://www.w3.org/TR/did-core/ !
*/

//> Dependencies!
import * as blake2 from 'blakejs';
import * as canonicalize from 'canonicalize';
import { base58 } from 'ethers/lib/utils.js';
//> Implements a Decentralized Identitifer (DID)!
class Did {

	//> Constructs a new DID!
	constructor (uuid, network = "main", method = "iota", standard = "did", seperator = ":") {

		//> Check for construction out of an existing (full) did string!
		if (uuid.includes(seperator)) {
			this._seperator = seperator;
			const parts = uuid.split(seperator);
			this._standard = parts[0];
			this._method = parts[1];
			
			//> Network is optional, so we need to check for that and default!
			if (parts.length == 4) {
				this._network = parts[2];
				this._uuid = parts[3];
			} else {
				this._network = network;
				this._uuid = parts[2];
			}

		//> Else just construct normally!
		} else {
			this._standard = standard;
			this._method = method;
			this._network = network;
			this._uuid = uuid;
			this._seperator = seperator;
		};

	};

	//> Construct from string!
	static fromString(s){
		return new Did(s);
	};

	//> Construct from JSON!
	static fromJSON(j){
		return Did.fromString(j)
	};

	//> Creates a Did instance from a publicKey in uncompressed format without 0x!
	static fromPublicKey(publicKeyCryptoString) {
		return new Did(Did.publicKeyToUuid(publicKeyCryptoString));
	};
	
	//> Getters!
	get standard() {
		return this._standard;
	};
	get method() {
		return this._method;
	};
	get network() {
		return this._network;
	};
	get uuid() {
		return this._uuid;
	};
	get seperator() {
		return this._seperator;
	};
	
	//> Setters!
	set standard(v) {
		this._standard = v;
	};
	set method(v) {
		this._method = v;
	};
	set network(v) {
		this._network = v;
	};
	set uuid(v) {
		this._uuid = v;
	};
	set seperator(v) {
		this._seperator = v;
	};

	//> Creates an uuid / IOTA address out of a uncompressed public key. Can be used as "uuid" input in DID construction!
	//expect public key in uncompressed format without 0x!
	static publicKeyToUuid(publicKey) {

		const publicKeyX = "0x" + publicKey.slice(1,65);
		// console.log("Public Key X:" + publicKeyX);
		const publicKeyY = "0x" + publicKey.slice(66);
		// console.log("Public Key Y:" + publicKeyY);

		const publicKeyJWK = {
			'kty': 'EC', // external (property name)
        	'crv': 'secp256k1',
			'x': base58.encode(publicKeyX.toString()),
			'y': base58.encode(publicKeyY.toString()),
		};
				
		// Canonicalize the JWK object

		const publicKeyJWKcanonicalized = canonicalize(publicKeyJWK);

		// Hash the object

		
		const did = "0x"+blake2.blake2sHex(Buffer.from(JSON.stringify(publicKeyJWKcanonicalized)));
		

		// convert to base58

		const ftrStr = base58.encode(did);


		return ftrStr;

	};

	//> Creates a string representation out of the DID!
	toString() {
		var str = this._standard + this._seperator + this._method + this._seperator;
			str += this._network + this._seperator;
		str += this._uuid;
		return str;

	};

	//> Creates a JSON representation out of the DID!
	toJSON(){
		return this.toString();
	};

};

//> List of module exports!

 export {
	Did,
};
