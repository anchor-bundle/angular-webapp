/*
	Implements a Decentralized Identitifer (DID) Document following https://www.w3.org/TR/did-core/ !
*/

//> Dependencies!
import * as _errors from "./../err/c_err.js";
import * as _cryptographyWrapper from "./../wrappers/cryptographyWrapper.js";
import { Context as _Context } from "./Context.js";
import { Did as _Did } from "./Did.js";
import { Proof as _Proof } from "./Proof.js";
import { PublicKey as _PublicKey } from "./PublicKey.js";
import { Timestamp as _Timestamp } from "./Timestamp.js";

//> Schema dependencies!
import { schema as _DidDocumentSchema } from "./../schema/schema_DidDocument.js";

//> Implements a DID Document!
class DidDocument {

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Constructor and creation  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Creates a new "barebone" DID Document!
	constructor (contextObj, DidObj, createdObj, updatedObj, authenticationKeysObjList, publicKeysObjList) {

		//> DID Document data!
		this._data = {
			"@context": contextObj,
			"id": DidObj, //> DID Object!
			"created": createdObj, //> Follows https://www.w3.org/TR/NOTE-datetime and ISO 8601!
			"updated": updatedObj, //> Same!
			"authentication": authenticationKeysObjList, //> List of DID PublicKey objects!
			"publicKey": publicKeysObjList //> Same!
		};

	};

	//> Creates new DID Document from a public key (crypto) object!
	static fromPublicKey(publicKeyCryptoObject) {
		const contextObj = new _Context("https://www.w3.org/ns/did/v1");
		const DidObj = _Did.fromPublicKey(_cryptographyWrapper.publicKeyToString(publicKeyCryptoObject));
		const authenticationKeysObjList = [ //> See https://www.w3.org/TR/did-core/#public-keys !
			new _PublicKey(publicKeyCryptoObject, DidObj, DidObj, "auth-key-1", "Ed25519VerificationKey2018")
		];
		const publicKeysObjList = [
			new _PublicKey(publicKeyCryptoObject, DidObj, DidObj, "key-1", "Ed25519VerificationKey2018")
		];
		return new DidDocument(contextObj, DidObj, new _Timestamp(), new _Timestamp(), authenticationKeysObjList, publicKeysObjList);
	};

	//> Creates a (already given) DID Document object from a JSON object!
	static fromJSON(j) {

		//> Add each field individually!
		const contextObj = new _Context(j["@context"]);
		const DidObj = new _Did(j.id);
		const createdObj = new _Timestamp(j.created);
		const updatedObj = new _Timestamp(j.updated);
		const authenticationKeysObjList = [];

		//> List fields!
		for (var index in j.authentication) {
			authenticationKeysObjList.push(_PublicKey.fromJSON(j.authentication[index]));
		};
		const publicKeysObjList = [];
		for (var index in j.publicKey) {
			publicKeysObjList.push(_PublicKey.fromJSON(j.publicKey[index]));
		};
		var dd = new DidDocument(contextObj, DidObj, createdObj, updatedObj, authenticationKeysObjList, publicKeysObjList);

		//> Add proof if there is one!
		if(j.hasOwnProperty("proof")){
			dd.proof = _Proof.fromJSON(j.proof);
		}

		//> Return new DidDocument!
		return dd;

	};	

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Getters, Schema and Setters  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Getters!
	get context() {
		return this._data["@context"];
	};
	get id() {
		return this._data["id"];
	};
	get created() {
		return this._data["created"];
	};
	get updated() {
		return this._data["updated"];
	};
	get authentication() {
		return this._data["authentication"];
	};
	get publicKey() {
		return this._data["publicKey"];
	};
	get proof() {
		return this._data["proof"];
	};
	
	//> Setters!
	set context(v) {
		this._data["@context"] = v;
	};
	set id(v) {
		this._data["id"] = v;
	};
	set created(v) {
		this._data["created"] = v;
	};
	set updated(v) {
		this._data["updated"] = v;
	};
	set authentication(v) {
		this._data["authentication"] = v;
	};
	set publicKey(v) {
		this._data["publicKey"] = v;
	};
	set proof(v) {
		this._data["proof"] = v;
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Validation and verification  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Checks whether this DID Document's proof is valid!
	verify() {

		return DidDocument.verify(this);

	};

	//> Checks whether a given DID Document proof signature is valid!
	static verify(didDocumentObject) {

		//> Check for proof existance!
		if(didDocumentObject.proof === undefined){
			return false;
		};

		//> Initialize some variables!
		const publicKeyObject = didDocumentObject.proof.creator.resolve(didDocumentObject).publicKey;
		const signature = didDocumentObject.proof.signatureValue;
		const ddSinProof = didDocumentObject.toJSON(["proof"]);

		//> Verify!
		return _cryptographyWrapper.verify(ddSinProof, publicKeyObject, signature);
		
	};

	//> Syntactically validates the DID Document!
	validate(){
		return DidDocument.validate(this.toJSON(), _DidDocumentSchema);
	};

	

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//vvvvv  Auxiliary  vvvvv//
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//> Returns a stringify-able JSON representation of itself!
	toJSON(ignoreFields = []) {
		var json = {};
		for (var key in this._data) {
			if (!(ignoreFields.includes(key))) {
				switch (key) {
					case "authentication":
					case "publicKey":
						json[key] = [];
						for (var index in this._data[key]) {
							json[key].push(this._data[key][index].toJSON());
						};
						break;
					default:
						json[key] = this._data[key].toJSON();
						break;
				}
			}
		}
		return json;
	};

	//> Add cryptographic proof to DD!
	addProof(privateKey, creatorPublicKeyID = 0) {

		//> Check if there is a proof already!
		if(this._data.proof !== undefined){
			throw _errors.DD_PROOF_ERROR
		};

		const creatorPKR = this._data.publicKey[creatorPublicKeyID].id;
		const signatureValue = _cryptographyWrapper.sign(this.toJSON(), privateKey);
		this._data.proof = new _Proof(signatureValue, creatorPKR);

	};

};

//> List of module exports!
export {
	DidDocument,
};
