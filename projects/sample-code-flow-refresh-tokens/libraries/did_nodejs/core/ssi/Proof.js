/*
	Implements Proof field for storing signatures within DID Documents and VCs. Example:

	"proof": {
		"created": "2020-05-01T03:00:02Z", //> Timestamp!
		"creator": "did:example:12D3KooWMHdrzcwpjbdrZs5GGqERAvcgqX3b5dpuPtPa9ot69yew#keys-1", //> "Creator" refers to the public key used to create the proof, NOT to the DID!
		"signatureValue": "o9r6LxgoGN8FoaeeUA6EdDcv12GvDzFEmCgjWzvpur2YSQyA8W2r0SSWUK+nH5tMqzaFLun6wwZ1Eot37amGDg==", //> The actual signature!
		"type": "Ed25519Signature2018" //> Type of signature!
	},

*/

//> Dependencies!
import { base64 } from "ethers/lib/utils.js";
import { PublicKeyReference as _PublicKeyReference } from "./PublicKeyReference.js";
import { Timestamp as _Timestamp } from "./Timestamp.js";

class Proof {

	//> Creates a new Proof object!
	// important: the signature value is encoded base64 in hex format
	// if you want to decode it you need to convert the bytes to hex to get the original value
	constructor (signatureValue, creatorPKR, created = undefined, type = "JsonWebSignature2020", proofPurpose = undefined) {
		const jwsHeader = {
			'alg':'ES256K',
			'b64':false,
			'crit':['b64'],
		};
		const signature = base64.encode(Array.from(Buffer.from(JSON.stringify(jwsHeader)))) + ".." + base64.encode(signatureValue);
		this._signatureValue = signature;
		this._creator = creatorPKR; //> PublicKeyReference Object!
		this._type = type; //> E.g. "RsaVerificationKey2018" or "Ed25519VerificationKey2018" or "Ed25519Signature2018"!
		if (created == undefined) {
			this._created = new _Timestamp(); //> Timestamp following https://www.w3.org/TR/NOTE-datetime and ISO 8601!
		} else {
			this._created = new _Timestamp(created);
		};
		this._proofPurpose = proofPurpose; //> For VCs only, there mostly "assertionMethod"!

	};

	//> Creates a proof object from JSON!
	static fromJSON(j) {
		return new Proof(j.signatureValue || j.jws, _PublicKeyReference.fromJSON(j.creator || j.verificationMethod), j.created, j.type, j.proofPurpose);
	};
	
	//> Getters!
	get signatureValue() {
		return this._signatureValue;
	};
	get creator() {
		return this._creator;
	};
	get type() {
		return this._type;
	};
	get created() {
		return this._created;
	};
	get proofPurpose() {
		return this._proofPurpose;
	};

	//> Setters!
	set signatureValue(v) {
		this._signatureValue = v;
	};
	set creator(v) {
		this._creator = v;
	};
	set type(v) {
		this._type = v;
	};
	set created(v) {
		this._created = v;
	};
	set proofPurpose(v) {
		this._proofPurpose = v;
	};
	
	//> Returns a stringify-able JSON representation of itself!
	toJSON() {

		//> Export as a DID Document!
		if(this._proofPurpose == undefined){
			return {
				"type": this._type,
				"created": this._created.toString(),
				"creator": this._creator.toString(),
				"signatureValue": this._signatureValue
			}
		};

		//> Export as a VC!
		return {
			"type": this._type,
			"created": this._created.toString(),
			"proofPurpose": this._proofPurpose,
			"verificationMethod": this._creator.toString(), //> For VCs only, equals "creator"!
			"jws": this._signatureValue
		}
		
	};
	
};

//> List of module exports!
export {
	Proof,
};
