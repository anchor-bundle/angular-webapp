/*
	Wrapper for nodejs crypto library. Operates on key objects, not on strings!
*/

//> Dependencies!
import * as crypto from "crypto";
import * as _errors from "./../err/c_err.js";

//> Creates a new public/private key pair!
function generateKeyPair(type = "ed25519") {
	if (type == "rsa") {
		const { publicKey, privateKey } = crypto.generateKeyPairSync('rsa', {
			modulusLength: 4096
		});
		return {publicKey: publicKey, privateKey: privateKey};
	} else if (type == "ed25519"){
		const { publicKey, privateKey } = crypto.generateKeyPairSync('ed25519');
		return {publicKey: publicKey, privateKey: privateKey};
	} else {
		throw _errors.CRYPTOGRAPHYWRAPPER_INVALID_TYPE;
	};
};

//> Derives a public key from a given private key!
function derivePublicKey(privateKey) {
	return crypto.createPublicKey(privateKey);
};

//> Returns the signature of signing some data with a given private key!
function sign(dataJSON, privateKey, encoding = "base64") {
	return crypto.sign(null, Buffer.from(JSON.stringify(dataJSON)), privateKey).toString(encoding);
};

//> Verifies the correctness of some signature given a public key object, the signature and the data that was signed!
function verify(dataJSON, publicKey, signature, encoding = "base64") {
	return crypto.verify(null, Buffer.from(JSON.stringify(dataJSON)), publicKey, Buffer.from(signature, encoding));
};

//> Exports a public key object to a string!
function publicKeyToString(pubKeyObject){
	return pubKeyObject.export({"type": "spki", "format": "pem"});
};

//> Exports a private key object to a string!
function privateKeyToString(privKeyObject){
	return privKeyObject.export({"type": 'pkcs8', "format": 'pem'});
};

//> Imports a public key string into an object!
function stringToPublicKey(publicKeyPem){
	return crypto.createPublicKey({"key": publicKeyPem, "type": "spki", "format": "pem"});
};

//> Imports a private key string into an object!
function stringToPrivateKey(privateKeyPem){
	return crypto.createPrivateKey({"key": privateKeyPem, "type": "pkcs8", "format": "pem"});
};

//> Returns a random string!
function rstr(l){
	return crypto.randomBytes(l).toString("base64")
};

//> List of module exports!
 export {
	generateKeyPair,
	sign,
	derivePublicKey,
	verify,
	publicKeyToString,
	privateKeyToString,
	stringToPublicKey,
	stringToPrivateKey,
	rstr,
};
 