/*
	Wraps IOTA DLT functionalities!
*/

//> Dependencies!
import * as converter from "@iota/converter";
	



//> Converts ascii to trytes!
var asciiToTrytes = function(a) {
	return converter.asciiToTrytes(a);
};



//> Trims or fills a given tryte string to 81 characters!
var strfi = function(s){
	s = s.substr(0, 81); //> Trim to 81 characters (if too long)!
	s = s + new Array(81-s.length + 1).join("9"); //> Fill to 81 characters (if too short)!
	return s;
};

//> List of module exports!
export {
	asciiToTrytes,
	strfi,
};
