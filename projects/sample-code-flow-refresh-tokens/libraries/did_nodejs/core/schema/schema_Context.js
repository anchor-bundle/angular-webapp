/*
	Schema implementation for Context.js!
*/

const schema = {

	"id": "/ContextSchema",
	"type": "array", //> Norm to array!
	"items": {
		"type": "string"
	}

};

//> List of module exports!
export {
	schema,
};