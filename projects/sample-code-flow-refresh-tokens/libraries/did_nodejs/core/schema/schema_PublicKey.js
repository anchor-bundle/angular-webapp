/*
	Schema implementation for PublicKey.js!
*/

const schema = {

	"id": "/PublicKeySchema",
	"type": "array",
	"items": {
		"type": "object",
		"properties": {
			"id": {
				"$ref": "/PublicKeyReferenceSchema"
			},
			"type": {
				"type": "string"
			},
			"controller": {
				"$ref": "/DidSchema"
			},
			"publicKeyPem": {
				"type": "string",
				"pattern": "^-----BEGIN PUBLIC KEY-----\n.+\n-----END PUBLIC KEY-----\n$"
			}
		},
		"required": ["id", "type", "controller", "publicKeyPem"]
	}
	
};

//> List of module exports!
export {
	schema,
};