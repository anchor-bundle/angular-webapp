/*
	Schema implementation for Did.js!
*/

const schema = {

	"id": "/DidSchema",
	"type": "string",
	"pattern": "^[a-z]+:[a-z]+:([a-z]+:)?[A-Z9]+$"

};

//> List of module exports!
export {
	schema,
};