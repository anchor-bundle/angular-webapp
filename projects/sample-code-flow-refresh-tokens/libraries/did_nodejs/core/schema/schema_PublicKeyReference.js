/*
	Schema implementation for PublicKeyReference.js!
*/

const schema = {

	"id": "/PublicKeyReferenceSchema",
	"type": "string",
	"pattern": "^[a-z]+:[a-z]+:([a-z]+:)?[A-Z9]+#.+$"

};

//> List of module exports!
export {
	schema,
};