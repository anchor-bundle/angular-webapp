/*
	Schema implementation for VerifiableCredential.js!
*/

const schema = {

	"id": "/VerifiableCredentialSchema",
	"type": "object",
	"properties": {
		"@context": {
			"$ref": "/ContextSchema"
		},
		"id": {
			"$ref": "/UriSchema" //> Technically not a DID!
		},
		"type": {
			"type": "array",
			"items": {
				"type": "string"
			}
		},
		"issuer": {
			"$ref": "/DidSchema"
		},
		"issuanceDate": {
			"$ref": "/TimestampSchema"
		},
		"proof": {
			"$ref": "/ProofSchema"
		},
		"credentialSubject": {
			"type": "object",
			"properties": {
				"id": {
					"$ref": "/DidSchema"
				}
			},
			"required": ["id"]
		},
	},
	"required": ["@context", "id", "type", "issuer", "issuanceDate", "proof", "credentialSubject"]

};

//> List of module exports!
export {
	schema,
};