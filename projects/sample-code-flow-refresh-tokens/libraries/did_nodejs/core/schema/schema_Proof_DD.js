/*
	Schema implementation for Proof.js (DDs)!
*/

const schema = {

	"id": "/ProofSchema",
	"type": "object",
	"properties": {
		"type": {
			"type": "string"
		},
		"created": {
			"$ref": "/TimestampSchema"
		},
		"creator": {
			"$ref": "/PublicKeyReferenceSchema"
		},
		"signatureValue": {
			"type": "string"
		}
	},
	"required": ["type", "created", "creator", "signatureValue"]

};

//> List of module exports!
export {
	schema,
};