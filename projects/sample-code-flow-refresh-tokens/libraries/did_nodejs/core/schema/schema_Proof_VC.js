/*
	Schema implementation for Proof.js (VCs)!
*/

const schema = {

	"id": "/ProofSchema",
	"type": "object",
	"properties": {
		"type": {
			"type": "string"
		},
		"created": {
			"$ref": "/TimestampSchema"
		},
		"proofPurpose": {
			"type": "string"
		},
		"verificationMethod": {
			"$ref": "/PublicKeyReferenceSchema"
		},
		"jws": {
			"type": "string"
		}
	},
	"required": ["type", "created", "proofPurpose", "verificationMethod", "jws"]

};

//> List of module exports!
export {
	schema,
};