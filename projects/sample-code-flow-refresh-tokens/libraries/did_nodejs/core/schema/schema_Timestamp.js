/*
	Schema implementation for Timestamp.js!
*/

const schema = {

	"id": "/TimestampSchema",
	"type": "string",
	"pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{1,3}Z$"

};

//> List of module exports!
export {
	schema,
};