/*
	Schema implementation for Uri.js!
*/

const schema = {

	"id": "/UriSchema",
	"type": "string"

};

//> List of module exports!
export {
	schema,
};