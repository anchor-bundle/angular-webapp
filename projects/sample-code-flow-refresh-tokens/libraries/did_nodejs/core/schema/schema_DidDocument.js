/*
	Schema implementation for DidDocument.js!
*/

const schema = {

	"id": "/DidDocumentSchema",
	"type": "object",
	"properties": {
		"@context": {
			"$ref": "/ContextSchema"
		},
		"id": {
			"$ref": "/DidSchema"
		},
		"created": {
			"$ref": "/TimestampSchema"
		},
		"updated": {
			"$ref": "/TimestampSchema"
		},
		"authentication": {
			"$ref": "/PublicKeySchema"
		},
		"publicKey": {
			"$ref": "/PublicKeySchema"
		},
		"proof": {
			"$ref": "/ProofSchema"
		},
	},
	"required": ["@context", "id", "created", "updated", "publicKey", "proof"] //> Authentication is technically not requried!

};

//> List of module exports!
export {
	schema,
};