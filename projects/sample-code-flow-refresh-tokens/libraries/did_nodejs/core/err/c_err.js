/*
	Defines error constants!
*/

//> CryptographyWrapper!
const CRYPTOGRAPHYWRAPPER_INVALID_TYPE = "Error: CryptographyWrapper: Invalid key type!";

//> IotaWrapper!
const IOTAWRAPPER_UPLOAD_ERROR = "Error: IotaWrapper: Upload failed!";
const IOTAWRAPPER_DOWNLOAD_ERROR = "Error: IotaWrapper: Download failed!";

//> VC!
const VC_PROOF_ERROR = "Error: Vc: Can't add proof: Is already added!"
const VC_DRAGONS = "Error: Vc: Hic sunt dracones!";

//> DD!
const DD_PROOF_ERROR = "Error: DidDocument: Can't add proof: Is already added!"

//> DID!
const DID_KEY_CONVERSION_ERROR = "Error: Did: Key to UUID conversion failed!";

//> List of module exports!
export {
	CRYPTOGRAPHYWRAPPER_INVALID_TYPE,
	IOTAWRAPPER_UPLOAD_ERROR,
	IOTAWRAPPER_DOWNLOAD_ERROR,
	VC_PROOF_ERROR,
	VC_DRAGONS,
	DD_PROOF_ERROR,
	DID_KEY_CONVERSION_ERROR,
};