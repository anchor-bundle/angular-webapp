import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ImportVcComponent } from './import-vc.component';


describe('ImportVcComponent', () => {
  let component: ImportVcComponent;
  let fixture: ComponentFixture<ImportVcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ImportVcComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ImportVcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
