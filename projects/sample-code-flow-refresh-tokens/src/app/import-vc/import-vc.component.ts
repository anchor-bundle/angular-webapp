import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Vc } from '../../../libraries/did_nodejs/core/ssi/Vc.js';
import { LocalStorageManagementService } from '../services/local-storage-management.service';
import { AlertService } from '../_alert/alert.service';

@Component({
  selector: 'app-import-vc',
  templateUrl: './import-vc.component.html',
  styleUrls: ['./import-vc.component.css']
})
export class ImportVcComponent implements OnInit {
  textArea: string;
  constructor(private router: Router, private localStorageManagement: LocalStorageManagementService, private alertService: AlertService) { }

  ngOnInit(): void {
  }

  /*
  import an existing VC into the localstorage
  the format of the VC must be the same as the VC class
  after successful saving the VC the User is redirected to the home page
  */
  importFromTextarea() {
    if (this.textArea) {
      // convert to VC Object
      try {
        let vc: Vc = Vc.fromJSON(JSON.parse(this.textArea));
        //save VC in Localstorage
        this.localStorageManagement.saveVC(JSON.stringify(vc));
        this.alertService.success("imported VC successful", { keepAfterRouteChange: true });
        this.router.navigate(['home']);
      } catch (error) {
        this.alertService.error("error in paring VC. Maybe wrong format");
      }
    } else {
      this.alertService.warn("no value is pasted into the textfield!");
    }
  }
  //TODO: testen, wenn zeit ist.
  cancel() {
    this.router.navigate(['home']);
  }
}
