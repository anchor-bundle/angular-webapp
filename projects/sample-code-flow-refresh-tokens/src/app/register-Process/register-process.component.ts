import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import WalletConnect from '@walletconnect/client';
import canonicalize from 'canonicalize';
import { Vc } from '../../../libraries/did_nodejs/core/ssi/Vc.js';
import { WalletNotConnectedError } from '../errors/WalletNotConnectedError';
import { DataService } from '../services/data.service';
import { DidCreatorService } from '../services/did-creator.service';
import { FilancoreApiService } from '../services/filancore-api.service';
import { LocalStorageManagementService } from '../services/local-storage-management.service';
import { PublicKeyService, PublicKeyType } from '../services/public-key.service';
import { VcCreatorService } from '../services/vc-creator.service';
import { ConnecetToWalletService } from '../services/walletconnect.service';
import { AlertService } from '../_alert';
@Component({
    selector: 'app-register-process',
    templateUrl: './register-process.component.html',
    styleUrls: ['./register-process.component.css']
})
export class RegisterProcessComponent implements OnInit {

    //checkboxs in the html
    box1: boolean = false;
    box2: boolean = false;
    box3: boolean = false;
    box4: boolean = false;
    box5: boolean = false;
    box6: boolean = false;
    box7: boolean = false;
    box8: boolean = false;
    box9: boolean = false;
    box10: boolean = false;
    //indeterminate Boxes in the html
    indeterminateBoxes = [false, false, false, false, false, false, false, false, false, false];
    vcToDisplay = '';
    didDocToDisplay = '';
    registrationFinish: boolean = false; // enable toLogin Button if registration is finsish
    connector: WalletConnect;
    constructor(private walletconnect: ConnecetToWalletService, private extractPublicKey: PublicKeyService, private dataservice: DataService,
        private vcCreator: VcCreatorService, private alertService: AlertService, private localStorageService: LocalStorageManagementService, private didCreator: DidCreatorService, private filancoreApi: FilancoreApiService, private router: Router) { }

    ngOnInit(): void {
        //start registration Process
        console.log("ngoninit registerprocess");

        this.startProcess();
    }

    async startProcess() {
        console.log("start Register process");

        try {
            //STEP 1: connect to User Wallet
            this.connector = await this.walletconnect.initCommunication();
            if (this.connector.connected) {
                console.log("in der connected");
                this.box1 = true;
                //STEP 2:  let the User sign a message
                // the message is a Timestamp
                const message: string = Date.now().toString();
                const signedMessage: string = await this.letUserSign(this.connector, message);
                this.box2 = true;

                //STEP 3: extrakt the public key from the signed Message
                const publicKeyUser: string = this.extractPublicKey.getPublicKey(message, signedMessage, PublicKeyType.uncompressed);
                console.log("public key user: " + publicKeyUser);

                this.box3 = true;

                //STEP 4: createDID Document 
                let didDocument = this.didCreator.createDIDDocument(publicKeyUser);
                this.box4 = true;
                console.log(didDocument);
                //STEP 5: let the user sign the DID document
                const signature: string = await this.letUserSign(this.connector, canonicalize(didDocument));
                this.box5 = true;
                this.didCreator.addProof(didDocument, signature);
                this.didDocToDisplay = JSON.stringify(didDocument, null, 4);

                //STEP 6: upload Document to Tangle
                await this.filancoreApi.publishDidDocument(didDocument);
                this.box6 = true;
                //STEP 7 get claims and generate VC
                let vc: Vc = await this.createVC(publicKeyUser);
                this.box7 = true;

                // STEP 8 let the User sign the generated VC => proof
                let signedVc: string = await this.letUserSign(this.connector, canonicalize(vc.toJSON(["proof"])));
                this.box8 = true;

                //STEP 9 add signed VC as proof to the VC
                this.vcCreator.addProof(vc, signedVc);
                this.box9 = true;

                //STEP 10 save VC in Browser
                this.localStorageService.saveVC(JSON.stringify(vc.toJSON()));
                this.box10 = true;
                //display VC to User
                this.vcToDisplay = JSON.stringify(vc.toJSON(), null, 4);
                this.registrationFinish = true;
                this.alertService.success("successfully registered", { autoClose: true });
                //Todo: nochmal anschauen ob der alert richtig geht
            } else {
                console.log("not connected");

                this.indeterminateBoxes[0] = false;
                throw new WalletNotConnectedError();
            }
        } catch (error) {
            console.log("error");
            console.log(error);
            this.setNextCheckboxfailed();
        }
    }


    /*
    let the user sign a message
    and display the user a info message, that his interaction is needed
    parameters:
    connector - active Walletconnect connection to the user wallet
    message - string which needs to me signed
    return:
    signed message(signed from user)
    errors are catched in the startProcess method
    */
    async letUserSign(connector: WalletConnect, message: string): Promise<string> {
        this.alertService.info("Please open your wallet and sign the message");
        let signedData: string;
        await this.walletconnect.signMessage(message, connector).then((signedMessage) => {
            signedData = signedMessage;
        });
        return signedData;
    }
    /*
    get claims from dataservice (which User filled in the registration form)
    and then calls the method generateVC which generates the VC from a publickey and claims
    parameters:
    publicKey - public key from user
                format: uncompressed
    return:
    VC- verifiable credentials with claims from user
    throws error if claims are not available
    */
    async createVC(publicKey: string): Vc {
        let claims;
        //get claims from dataservice
        await this.dataservice.claims.subscribe((data) => {
            claims = data;
        });
        if (claims) {
            let vc = this.vcCreator.generateVC(null, publicKey, claims);
            if (vc) {
                return vc;
            } else {
                throw new Error("Error while creating VC");
            }
        } else {
            throw new Error('could not get claims');
        }
    }
    //redirect the user to the login, if registration is finish
    redirectToLogin() {
        console.log('bin in der redirect');
        this.router.navigate(['login']);
    }
    /*
    this function is called if an error occures
    this function checks at wich step the process failed and highlight the depending checkbox
    */
    setNextCheckboxfailed() {
        let checkboxes = [this.box1, this.box2, this.box3, this.box4, this.box5, this.box6, this.box7, this.box8, this.box9, this.box10];
        if (this.box1 == false) {
            this.indeterminateBoxes[0] = true;
        } else {
            for (let index = 0; index < checkboxes.length; index++) {
                if (checkboxes[index] === true && checkboxes[index + 1] === false) {
                    this.indeterminateBoxes[index + 1] = true;
                }
            }
        }
    }
    /*
    this is called if the user is already connected and want to connect to a different wallet
    */
    async connectToNewWallet() {
        console.log("connect to new Wallet");
        await this.connector.killSession();// diconnect from wallet
        this.resetCheckboxes();
        this.startProcess();

    }
    /*
    unchecks every checkbox
    */
    resetCheckboxes() {
        this.box1 = false;
        this.box2 = false;
        this.box3 = false;
        this.box4 = false;
        this.box5 = false;
        this.box6 = false;
        this.box7 = false;
        this.box8 = false;
        this.box9 = false;
        this.box10 = false;
        //indeterminate Boxes in the html
        for (let index = 0; index < this.indeterminateBoxes.length; index++) {
            this.indeterminateBoxes[index] = false;

        }

    }
}
