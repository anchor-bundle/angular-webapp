import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RouterTestingModule } from '@angular/router/testing';
import canonicalize from 'canonicalize';
import { DataService } from '../services/data.service';
import { AlertModule } from '../_alert';
import { RegisterProcessComponent } from './register-process.component';

describe('RegisterProcessComponent', () => {
    let component: RegisterProcessComponent;
    let fixture: ComponentFixture<RegisterProcessComponent>;
    let dataService: DataService;
    let publicKeyPem: string;
    let publicKeyUncompressed: string;
    let claims;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule, MatCheckboxModule, FormsModule, AlertModule],
            declarations: [RegisterProcessComponent],
            providers: [DataService],
        })
            .compileComponents();

    });
    beforeEach(() => {
        dataService = TestBed.inject(DataService);
        RegisterProcessComponent.prototype.ngOnInit = () => { };
        fixture = TestBed.createComponent(RegisterProcessComponent);
        component = fixture.componentInstance;
        publicKeyUncompressed = '046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';
        publicKeyPem = '-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEbVASo1FaXDJcMvrwJuXzlDsvsH67nV6T\nFw5g2sqSeWGnOs58iNs72kMQydsteyzSNgN0vdT9C3+dZd6H/5gkXA==\n-----END PUBLIC KEY-----';
        claims = [
            { "claimName": "firstName", "claimContent": "max" },
            { "claimName": "lastName", "claimContent": "mustermann" },
            { "claimName": "nickname", "claimContent": "m.mustermann" },
            { "claimName": "town", "claimContent": "berlin" },
            { "claimName": "email", "claimContent": "m.muster@gmail.com" }
        ];
        dataService.changeClaims(claims);
        fixture.detectChanges();

    })


    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('createVC should create VC', async () => {
        expect(await component.createVC(publicKeyUncompressed)).toBeTruthy();
    });
    it('createVC should throw Error if public key is in the wrong format', async () => {
        await expectAsync(component.createVC(publicKeyPem)).toBeRejectedWith(new Error('public Key is in the wrong format... should be uncompressed'));

    });
    it('createVC should contain claims', async () => {
        let vc = await component.createVC(publicKeyUncompressed);
        expect(claims).toEqual(jasmine.objectContaining(vc._data.credentialSubject.claims));
    });
});
describe('RegisterProcessComponent Without Claims', () => {
    let component: RegisterProcessComponent;
    let fixture: ComponentFixture<RegisterProcessComponent>;
    let publicKeyPem: string;
    let publicKeyUncompressed: string;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule, MatCheckboxModule, FormsModule, AlertModule],
            declarations: [RegisterProcessComponent],
            providers: [DataService],
        })
            .compileComponents();
    });
    beforeEach(() => {
        RegisterProcessComponent.prototype.ngOnInit = () => { };
        fixture = TestBed.createComponent(RegisterProcessComponent);
        component = fixture.componentInstance;
        publicKeyPem = '-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEbVASo1FaXDJcMvrwJuXzlDsvsH67nV6T\nFw5g2sqSeWGnOs58iNs72kMQydsteyzSNgN0vdT9C3+dZd6H/5gkXA==\n-----END PUBLIC KEY-----';
        publicKeyUncompressed = '0x046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';
        fixture.detectChanges();
    })

    it('createVC should throw Error if claims are not available', async () => {
        await expectAsync(component.createVC(publicKeyUncompressed)).toBeRejectedWith(new Error('could not get claims'));
    });
    it('test canonicalize function', () => {
        const json = {
            "1": { "f": { "f": "hi", "F": 5 }, "\n": 56.0 },
            "10": {},
            "": "empty",
            "a": {},
            "111": [{ "e": "yes", "E": "no" }],
            "A": {}
        }
        expect(canonicalize(json)).toBe('{"":"empty","1":{"\\n":56,"f":{"F":5,"f":"hi"}},"10":{},"111":[{"E":"no","e":"yes"}],"A":{},"a":{}}');
    });
});
