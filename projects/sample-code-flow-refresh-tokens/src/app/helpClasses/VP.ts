import { Context as _Context } from 'projects/sample-code-flow-refresh-tokens/libraries/did_nodejs/core/ssi/Context.js';
import { Proof } from 'projects/sample-code-flow-refresh-tokens/libraries/did_nodejs/core/ssi/Proof.js';
import { Timestamp } from 'projects/sample-code-flow-refresh-tokens/libraries/did_nodejs/core/ssi/Timestamp.js';
import { Vc } from '../../../libraries/did_nodejs/core/ssi/Vc.js';
export class VP {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //vvvvv  Construction  vvvvv//
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    _data;
    //> Creates a new VP from scratch!
    constructor(
        vc: Vc
    ) {
        let context: string = "https://www.w3.org/2018/credentials/v1"; //> The (list of) context(s) for this VC!
        let type = ["VerifiablePresentation"];
        let timestamp: Timestamp = new Timestamp(undefined);
        let proof: Proof = undefined; //> The complete Proof object, in case we construct from JSON!
        //> Fill in data!
        this._data = {};
        this._data.context = new _Context(context); //> @context attribute!
        this._data.type = type;
        this._data.timestamp = timestamp;
        this._data.vc = vc;
        this._data.proof = proof;


    };


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //vvvvv  Getters, Setters  vvvvv//
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    //> Getters!
    get context() {
        return this._data.context;
    };
    get type() {
        return this._data.type;
    };
    get proof() {
        return this._data.proof;
    };

    //> Setters!
    set context(v) {
        this._data.context = v;
    };
    set type(v) {
        this._data.type = v;
    };
    set proof(v) {
        this._data.proof = v;
    };

    //> Returns a stringify-able JSON representation of itself!
    toJSON(ignoreFields = []) {
        var json = {};
        for (var key in this._data) {
            if (!(ignoreFields.includes(key))) {
                switch (key) {
                    case "proof":
                        if (this._data[key] != undefined) {
                            json[key] = this._data[key].toJSON();
                        };
                        break;
                    case "context":
                        json["@context"] = this._data[key].toJSON(true); //> Force list for context attribute, since this is a VC!
                        break;
                    case "vc":
                        json['verifiableCredential'] = [this._data[key].toJSON()];
                        break;
                    default:
                        json[key] = this._data[key];
                        break;
                }
            }
        }
        return json;
    };

};