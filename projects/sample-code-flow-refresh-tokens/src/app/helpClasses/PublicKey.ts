/*
    Implements a publicKey object in the context of Decentralized Identitifer (DID) following https://www.w3.org/TR/did-core/ !
    See also https://www.w3.org/TR/did-core/#example-15-various-public-keys . Note: This is _not_ an arbitrary public key, but the DID Document Object!
    Note also that this can also be used for the "authentication" part of a DID Document!
*/

//> Dependencies!
import { base58 } from 'ethers/lib/utils.js';
import { Did } from 'projects/sample-code-flow-refresh-tokens/libraries/did_nodejs/core/ssi/Did.js';
import { PublicKeyReference } from "projects/sample-code-flow-refresh-tokens/libraries/did_nodejs/core/ssi/PublicKeyReference.js";


export class PublicKey {

    _id: PublicKeyReference;
    _type: string;
    _controller: Did;
    _publicKey;
    //> Constructs a new PublicKey object!
    //expect public key in uncompressed format
    constructor(publicKey, constrollerDidObject, didObject, suffix, type = "Ed25519VerificationKey2018", seperator = "#") {

        const publicKeyX = "0x" + publicKey.slice(2, 66);
        const publicKeyY = "0x" + publicKey.slice(66);


        const publicKeyJWK = {
            'kty': 'EC', // external (property name)
            'crv': 'secp256k1',
            'x': base58.encode(publicKeyX.toString()),
            'y': base58.encode(publicKeyY.toString()),
        };
        this._id = new PublicKeyReference(didObject, suffix, seperator); //> PublicKeyReference == "Full Name" of the PublicKey object, e.g. did:iota:main:AZHGSDJ...JHAGSDJH#key-1 !
        this._type = type; //> E.g. "Ed25519VerificationKey2018" or "RsaVerificationKey2018"!
        this._controller = constrollerDidObject; //> Controller of key's DID Object!
        this._publicKey = publicKeyJWK; //> The actual key in JWK format!
    };

    //> Creates a public key or authentication object from JSON!
    /*     static fromJSON(j, sep = "#") {
            var splitID = j.id.split(sep);
            return new PublicKey(_cryptographyWrapper.stringToPublicKey(j.publicKeyPem), new Did(j.controller), new Did(splitID[0]), splitID[1], j.type, sep);
        };
     */
    //> Getters!
    get id() {
        return this._id;
    };
    get type() {
        return this._type;
    };
    get controller() {
        return this._controller;
    };
    get publicKey() {
        return this._publicKey;
    };

    //> Setters!
    set id(v) {
        this._id = v;
    };
    set type(v) {
        this._type = v;
    };
    set controller(v) {
        this._controller = v;
    };
    set publicKey(v) {
        this._publicKey = v;
    };

    //> Returns a stringify-able JSON representation of itself!
    toJSON() {
        return {
            "id": this._id.toString(),
            "type": this._type,
            "controller": this._controller.toString(),
            "publicKeyJwk": this._publicKey
        };
    };

};
