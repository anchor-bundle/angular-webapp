import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { VcCreatorService } from '../services/vc-creator.service';
import { RegisterComponent } from './register.component';


describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [RegisterComponent],
      providers: [VcCreatorService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('get Claims correct', () => {
    const fname: string = "Max";
    const lname: string = "Mustermann";
    const nickname: string = "maxM";
    const town: string = "Berlin";
    const email: string = "max.M@gmail.com";
    const claims = component.getClaimsFormat(fname, lname, nickname, town, email);
    expect(claims).toContain({ "claimName": "firstName", "claimContent": fname })
    expect(claims).toContain({ "claimName": "lastName", "claimContent": lname });
  });
});
