import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { VcCreatorService } from '../services/vc-creator.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private vcCreator: VcCreatorService, private router: Router, private dataservice: DataService) { }

  ngOnInit(): void {
  }

  /*
  this method is called when user submits the registration form
  */
  registerUser(form: NgForm) {
    const fname: string = form.value['fname'];
    const lname: string = form.value['lname'];
    const nickname: string = form.value['nickname'];
    const town: string = form.value['town'];
    const email: string = form.value['email'];
    if (fname && lname && nickname && town && email) {
      //transform input parameters to the claims format
      let claims = this.getClaimsFormat(fname, lname, nickname, town, email);
      // save claims for the VC creation later
      this.saveClaims(claims);
      this.router.navigate(['register/process']);
    } else {
      alert("bitte füllen sie jeden Wert aus");

    }

  }

  getClaimsFormat(fname: string, lname: string, nickname: string, town: string, email: string) {
    return [
      { "claimName": "firstName", "claimContent": fname },
      { "claimName": "lastName", "claimContent": lname },
      { "claimName": "nickname", "claimContent": nickname },
      { "claimName": "town", "claimContent": town },
      { "claimName": "email", "claimContent": email }
    ];
  }

  saveClaims(claims) {
    this.dataservice.changeClaims(claims);
  }

}


