import { TestBed } from '@angular/core/testing';
import { PublicKeyService, PublicKeyType } from './public-key.service';


describe('ExtractPublicKeyService', () => {
  let service: PublicKeyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PublicKeyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should throw an error if input parameters are invalid', () => {
    expect(function () { service.getPublicKey(null, null, PublicKeyType.compressed); }).toThrow(new Error("invalid input Parameter"));
  });
  it('should calculate uncompressed public Key correct', () => {
    let message: string = 'test';
    let signedMessage: string = '0xd6b525106ef38260337396a07944dc6340bb99d3ebebe1962445fb343df6f5e131e92c013d3f406bef616dbb02abfaefb8f773dc0ef859f2a9d9b13d3e937ab11b';
    expect(service.getPublicKey(message, signedMessage, PublicKeyType.uncompressed)).toBe('046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c');
  });
  it('should calculate pem public Key correct', () => {
    let message: string = 'test';
    const publicKeyPem = '-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEbVASo1FaXDJcMvrwJuXzlDsvsH67nV6T\nFw5g2sqSeWGnOs58iNs72kMQydsteyzSNgN0vdT9C3+dZd6H/5gkXA==\n-----END PUBLIC KEY-----';
    let signedMessage: string = '0xd6b525106ef38260337396a07944dc6340bb99d3ebebe1962445fb343df6f5e131e92c013d3f406bef616dbb02abfaefb8f773dc0ef859f2a9d9b13d3e937ab11b';
    expect(service.getPublicKey(message, signedMessage, PublicKeyType.pem)).toBe(publicKeyPem);
  });
  it('should calculate compressed public Key correct', () => {
    let message: string = 'test';
    const publicKeyCompressed = '026d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961';
    let signedMessage: string = '0xd6b525106ef38260337396a07944dc6340bb99d3ebebe1962445fb343df6f5e131e92c013d3f406bef616dbb02abfaefb8f773dc0ef859f2a9d9b13d3e937ab11b';
    expect(service.getPublicKey(message, signedMessage, PublicKeyType.compressed)).toBe(publicKeyCompressed);
  });

  it('should uncrompressed public key (in hex) correct to pem format', () => {
    const publicKeyPem = '-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEbVASo1FaXDJcMvrwJuXzlDsvsH67nV6T\nFw5g2sqSeWGnOs58iNs72kMQydsteyzSNgN0vdT9C3+dZd6H/5gkXA==\n-----END PUBLIC KEY-----';
    const publicKeyUncompressed = '0x046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';
    expect(service.convertPublicKeyToPEM(publicKeyUncompressed)).toBe(publicKeyPem);
  });
  it('should uncrompressed public key correct to pem format', () => {
    const publicKeyPem = '-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEbVASo1FaXDJcMvrwJuXzlDsvsH67nV6T\nFw5g2sqSeWGnOs58iNs72kMQydsteyzSNgN0vdT9C3+dZd6H/5gkXA==\n-----END PUBLIC KEY-----';
    const publicKeyUncompressed = '046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';
    expect(service.convertPublicKeyToPEM(publicKeyUncompressed)).toBe(publicKeyPem);
  });

  it('should not accept public key in the wrong fomrat(not 0x04)', () => {
    //0x05 is not correct, should be 0x04
    const publicKeyWrongFormat = '056d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';
    expect(function () { service.convertPublicKeyToPEM(publicKeyWrongFormat); }).toThrow(new Error('public key is in the wrong format'));
  });
  it('should not accept public key in the wrong fomrat(to short)', () => {
    //key is one bit to short
    const publicKeyWrongFormat = '046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4f0b7f9d65de87ff98245c';
    expect(function () { service.convertPublicKeyToPEM(publicKeyWrongFormat); }).toThrow(new Error('public key is in the wrong format'));
  });
  it('checkFormat wrong input parameters', () => {
    expect(function () { service.checkFormat(""); }).toThrow(new Error("wrong input parameters"));
  });
  it('checkFormat could not detect format', () => {
    expect(function () { service.checkFormat("0x0523432"); }).toThrow(new Error("could not detect Public Key format"));
  });
  it('checkFormat detect uncompressed key', () => {
    expect(service.checkFormat("0x0443873487438732899238")).toBe(PublicKeyType.uncompressed);
  });
  it('checkFormat detect uncompressed key without 0x', () => {
    expect(service.checkFormat("0443873487438732899238")).toBe(PublicKeyType.uncompressed);
  });
});
