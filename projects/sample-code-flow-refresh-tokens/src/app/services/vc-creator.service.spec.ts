import { TestBed } from '@angular/core/testing';
import { VcCreatorService } from './vc-creator.service';

describe('VcCreatorService', () => {
  let service: VcCreatorService;
  let claims;
  let didSubject: string;
  let publicKeyUncompressed: string;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VcCreatorService);
    publicKeyUncompressed = '046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';
    claims = [
      { "claimName": "firstName", "claimContent": "max" },
      { "claimName": "lastName", "claimContent": "mustermann" },
      { "claimName": "nickname", "claimContent": "m.mustermann" },
      { "claimName": "town", "claimContent": "berlin" },
      { "claimName": "email", "claimContent": "m.muster@gmail.com" }
    ];
    didSubject = 'did:iota:main:8z9PcFEDNi1EX5U1ampkwvvbBXFA4D6NuDRs5GmRxohz';
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should throw an error if parameters are invalid', () => {
    expect(function () { service.generateVC('', '', ''); }).toThrow(new Error("generate VC failed. some input values are missing"));

  });
  it('should throw an error if parameters are invalid', () => {
    expect(function () { service.generateVC(null, null, null); }).toThrow(new Error("generate VC failed. some input values are missing"));

  });
  it('should throw an error if parameters are invalid', () => {
    expect(function () { service.generateVC(null, publicKeyUncompressed, null); }).toThrow(new Error("generate VC failed. some input values are missing"));

  });
  it('should create VC', () => {
    expect(service.generateVC(null, publicKeyUncompressed, claims)).toBeTruthy();
  });
  it('vc should contain did of Subject', () => {
    expect(JSON.stringify(service.generateVC(null, publicKeyUncompressed, claims))).toContain(didSubject);
  });
  it('vc should contain claims', () => {
    let vc = service.generateVC(null, publicKeyUncompressed, claims);
    let _data = vc['_data'];
    let credentialSubject = _data['credentialSubject'];
    let vcClaims = credentialSubject['claims']
    expect(claims).toEqual(jasmine.objectContaining(vcClaims));
  });

  it('should add proof to VC', () => {
    // generate VC
    let vc = service.generateVC(null, publicKeyUncompressed, claims);
    let signatureValue = '0xd6b525106ef38260337396a07944dc6340bb99d3ebebe1962445fb343df6f5e131e92c013d3f406bef616dbb02abfaefb8f773dc0ef859f2a9d9b13d3e937ab11b';
    service.addProof(vc, signatureValue);
    expect(vc._data.proof).toBeTruthy();
  });
  it('addProof throw error if parameters are missing', () => {
    // generate VC
    let vc = service.generateVC(null, publicKeyUncompressed, claims);
    expect(function () { service.addProof(vc, null); }).toThrow(new Error("missing input parameters"));

  });
  it('addProof throw error if parameters are missing', () => {
    // generate VC
    let vc = service.generateVC(null, publicKeyUncompressed, claims);
    expect(function () { service.addProof(null, null); }).toThrow(new Error("missing input parameters"));

  });
});
