import { Injectable } from '@angular/core';
import { Did } from 'projects/sample-code-flow-refresh-tokens/libraries/did_nodejs/core/ssi/Did.js';
import { Proof } from "../../../libraries/did_nodejs/core/ssi/Proof.js";
import { PublicKeyReference } from "../../../libraries/did_nodejs/core/ssi/PublicKeyReference.js";
import { Vc } from '../../../libraries/did_nodejs/core/ssi/Vc.js';
import { PublicKeyService, PublicKeyType } from './public-key.service';
@Injectable({
  providedIn: 'root'
})
export class VcCreatorService {

  constructor(private publicKeyService: PublicKeyService) { }

  /*
  generates VC with User claims
  parameters:
  publicKeyIssuer - public Key which is the issuer of the generated VC (can be the same than the Subject)
  publicKeySubject - public Key from the User
  publickey format: !!uncompressed without 0x at begin!!
  claims - claims with user data(from registration form)
  return value:
  VC - VC with claims and public key, but WITHOUT proof
  throws Error if input parameter is missing or invalid
  */
  generateVC(publicKeyissuer: string, publicKeySubject: string, claims): Vc {

    //if publicKey from Issuer is null the user is the issuer and subject
    if (publicKeyissuer == null) {
      publicKeyissuer = publicKeySubject;
    }
    if (publicKeySubject && claims) {
      //check if public key is in the correct format
      if (this.publicKeyService.checkFormat(publicKeySubject) == PublicKeyType.uncompressed && this.publicKeyService.checkFormat(publicKeyissuer) == PublicKeyType.uncompressed) {
        const didSubject = Did.fromPublicKey(publicKeySubject);
        const didIssuer = Did.fromPublicKey(publicKeyissuer);
        var vc = new Vc(
          didIssuer, //> The Did Object of the entity that issued the credential!
          didSubject, //> The Did Object of the entity that the credential was issued for, the subject!
          claims, //> List of {claimName:claimName, claimContent:claimContent} objects detailing claims about the subject!
          "https://www.w3.org/2018/credentials/v1", //> The (list of) context(s) for this VC!
          "http://example.com/credentials/" + Math.random().toString(4).substring(2, 6), //> The VC identifier - can be anything, really. Defaults to a random URI string!
          ["VerifiableCredential"], //> List of credential types declaring expected data within credential!
          undefined, //> The date of issuance for the credential, as a conforming string or undefined for "now"!
          undefined, //> The complete Proof object, in case we construct from JSON!
        );
        return vc;
      } else {
        throw new Error("public Key is in the wrong format... should be uncompressed");
      }
    } else {
      throw new Error("generate VC failed. some input values are missing");

    }



  }
  /* Add cryptographic proof to VC!
  parameters:
  - VC - verifbiable credentialw with User claims WITHOUT proof
  - signatureValue - signed Value of the VC(signed by the user)
  return 
  noting, vc is changed
  */
  addProof(vc: Vc, signatureValue: string): void {
    if (vc && signatureValue) {
      //> Check if there is a proof already!
      if (vc._data.proof !== undefined) {
        throw new Error('proof already exists!');
      };
      const creatorPKR = new PublicKeyReference(vc._data.issuer, 'key-1');
      vc._data.proof = new Proof(signatureValue, creatorPKR, undefined, undefined, "assertionMethod");
    } else {
      throw new Error("missing input parameters");
    }

  }
}
