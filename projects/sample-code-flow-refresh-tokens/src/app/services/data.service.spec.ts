import { TestBed } from '@angular/core/testing';
import { DataService } from './data.service';


describe('VssaveService', () => {
  let service: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save data', () => {
    service.changeClaims("test");
    let claims: string;
    service.claims.subscribe((result) => {
      claims = result;
      expect(claims).toBe('test');
    });
  });

});
