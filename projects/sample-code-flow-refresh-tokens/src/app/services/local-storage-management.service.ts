import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageManagementService {
  //saves data to the localStorage of the browser
  saveVC(vc: string) {
    localStorage.setItem('vc', vc);
  }
  //get data from localStorage of the browser
  getVC() {
    return localStorage.getItem('vc');
  }
  //removes a item from localstorage
  removeData(key: string) {
    localStorage.removeItem(key);
  }

  constructor() { }
}
