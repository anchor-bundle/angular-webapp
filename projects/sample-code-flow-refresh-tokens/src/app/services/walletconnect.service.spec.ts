import { TestBed } from '@angular/core/testing';
import { ConnecetToWalletService } from './walletconnect.service';


describe('ConnecetToWalletService', () => {
  let service: ConnecetToWalletService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnecetToWalletService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
