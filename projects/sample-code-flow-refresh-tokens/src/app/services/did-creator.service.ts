import { Injectable } from '@angular/core';
import { Context } from '../../../libraries/did_nodejs/core/ssi/Context.js';
import { Did } from '../../../libraries/did_nodejs/core/ssi/Did.js';
import { DidDocument } from '../../../libraries/did_nodejs/core/ssi/DidDocument.js';
import { Proof } from "../../../libraries/did_nodejs/core/ssi/Proof.js";
import { Timestamp } from '../../../libraries/did_nodejs/core/ssi/Timestamp.js';
import { PublicKey } from '../helpClasses/PublicKey';
import { PublicKeyService, PublicKeyType } from './public-key.service';
@Injectable({
  providedIn: 'root'
})
export class DidCreatorService {
  //> Add cryptographic proof to DD!
  addProof(didDocument: DidDocument, signature: string) {
    if (didDocument && signature) {
      //> Check if there is a proof already!
      if (didDocument._data.proof !== undefined) {
        throw new Error('proof already exists');
      }
      const creatorPKR = didDocument._data.publicKey[0].id;
      didDocument._data.proof = new Proof(signature, creatorPKR);
    } else {
      throw new Error("wrong input parameters");
    }

  }
  /*
  creates DID Document, based on a publicKey
  parameters
  publicKeyUser - (format: uncompressen wihtout 0x)
  returns
  created DID Document
  throws Error if input parameters are invalid
  */
  createDIDDocument(publicKeyUser: string): DidDocument {
    if (this.publicKeyService.checkFormat(publicKeyUser) === PublicKeyType.uncompressed) {
      const contextObj = new Context("https://www.w3.org/ns/did/v1");
      const DidObj = Did.fromPublicKey(publicKeyUser);
      const authenticationKeysObjList = [ //> See https://www.w3.org/TR/did-core/#public-keys !
        new PublicKey(publicKeyUser, DidObj, DidObj, "auth-key-1", "JsonWebKey2020")
      ];
      const publicKeysObjList = [
        new PublicKey(publicKeyUser, DidObj, DidObj, "key-1", "JsonWebKey2020")
      ];
      return new DidDocument(contextObj, DidObj, new Timestamp(), new Timestamp(), authenticationKeysObjList, publicKeysObjList);
    } else {
      throw new Error("invalid input parameters or wrong public Key format");
    }

  }
  constructor(private publicKeyService: PublicKeyService) { }
}
