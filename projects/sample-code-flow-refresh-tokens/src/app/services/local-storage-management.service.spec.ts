import { TestBed } from '@angular/core/testing';
import { LocalStorageManagementService } from './local-storage-management.service';


describe('LocalStorageManagementService', () => {
  let service: LocalStorageManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageManagementService);
    service.removeData("vc");
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should save and get something in local storage', () => {
    service.saveVC('test');
    expect(service.getVC()).toBe('test');
  });
  it('should get null if the requested value is not in the localStorage', () => {
    expect(service.getVC()).toBe(null);
  });
});
