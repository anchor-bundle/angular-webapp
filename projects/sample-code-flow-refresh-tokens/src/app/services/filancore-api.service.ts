import { Injectable } from '@angular/core';
import { AuthenticateApi } from '../models/authenticate.model';

@Injectable({
  providedIn: 'root'
})
export class FilancoreApiService {
  private baseURL = 'https://ig.filancore.net/api/v1'
  private authenticationDetails: AuthenticateApi;

  constructor() { }
  /*
  check if a access token to the API is already available
  and if it is not available call 'getAccessToken()' to request one
  returns:
  true if access token is now available
  throw error if there is an error while getting the token
  */
  async authenticate(): Promise<boolean> {
    //check if access token is already available
    if (this.authenticationDetails && this.authenticationDetails.accessToken) {
      console.log("data is available. No need to get token");
      return true;
    } else {
      await this.getAccessToken();
      //check if access token is from type bearer
      if (this.authenticationDetails.tokenType === "Bearer" && this.authenticationDetails.accessToken) {
        return true;
      } else {
        console.log("error in getting access token");
        throw new Error("error while getting access token");
      }
    }
  }
  /*
  sends Post Request with username and password to the API to get an access token
  the token is saved in the class object authenticationDetails.accessToken
  throws error if there is an error while getting access token
  */
  async getAccessToken() {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      "passwordIdentifier": "testing_4@filancore.net",
      "password": "t8LM$#tzEked",
      "method": "password"
    });

    await fetch(this.baseURL + "/auth/login", {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: "follow"
    })
      .then(response => response.json())
      .then((result) => {
        console.log(result);
        this.authenticationDetails = new AuthenticateApi().deserialize(result);
      })
      .catch((error) => {
        console.log('error', error)
        throw new Error('Error while getting access token');
      });
  }


  /*
  publish DID Document to the Tangle over the Filancore API
  parameters:
  - doc: signed Did Document that needs to be published
  throws Error if there occurs an Error while publishing
  */
  async publishDidDocumentFilancore(doc: string) {
    // need to authenticate to the API
    await this.authenticate();
    var myHeaders = new Headers();
    myHeaders.append("Access-Control-Allow-Origin", "*");
    const bearerToken = "Bearer " + this.authenticationDetails.accessToken;
    // add bearer Token to authenticate
    myHeaders.append("Authorization", bearerToken);
    myHeaders.append("Content-Type", "application/json");

    await fetch(this.baseURL + "/identities", {
      method: 'PUT',
      headers: myHeaders,
      body: doc,
      redirect: 'follow'
    })
      .then(response => response.text())
      .then(result => console.log(result))
      .catch((error) => {
        console.log('error', error)
        throw new Error('error while publishing Did Document to Tangle');
      });
  }
  /**
   * only to test until the filacore api works
   * @param doc 
   */
  async publishDidDocument(doc: string) {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Access-Control-Allow-Origin", "*");
    await fetch("http://localhost:5555/users/publishDIDDocument", {
      method: 'PUT',
      headers: myHeaders,
      body: JSON.stringify(doc),
      redirect: 'follow'
    })
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => { throw new Error("error in publishing did doc") });
  }
}
