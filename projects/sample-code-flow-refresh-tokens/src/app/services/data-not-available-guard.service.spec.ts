import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { DataNotAvailableGuardService } from './data-not-available-guard.service';
import { DataService } from './data.service';


describe('DataNotAvailableGuardService', () => {
  let service: DataNotAvailableGuardService;
  let router: Router;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [DataService],
    })
      .compileComponents();
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataNotAvailableGuardService);
    router = TestBed.inject(Router);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
