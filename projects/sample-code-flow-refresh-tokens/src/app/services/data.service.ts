import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private claimsSource = new BehaviorSubject<any>('');
  claims = this.claimsSource.asObservable();
  private userDataSource = new BehaviorSubject<any>(null);
  userdata = this.userDataSource.asObservable();
  constructor() { }

  /*
  this Service is able to exchange data between components.
  One component calls the changeClaims method and put some data in
  An other component can subsript the claims variable and get the stored value
  */
  changeClaims(claims) {
    this.claimsSource.next(claims);
  }

  changeUserdata(userData) {
    this.userDataSource.next(userData);
  }
}
