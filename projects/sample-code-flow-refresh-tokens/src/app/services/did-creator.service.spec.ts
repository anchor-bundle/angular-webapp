import { TestBed } from '@angular/core/testing';
import { DidDocument } from '../../../libraries/did_nodejs/core/ssi/DidDocument.js';
import { DidCreatorService } from './did-creator.service';
describe('DidCreatorService', () => {
  let service: DidCreatorService;
  let publicKeyUncompressed;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DidCreatorService);
    publicKeyUncompressed = '046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('createDIDDocument pass invalid parameter', () => {
    //key starts with 02 -> compressed key-> should not accept format
    expect(function () { service.createDIDDocument("022054845856868"); }).toThrow(new Error("invalid input parameters or wrong public Key format"));
  });
  it('createDIDDocument pass invalid parameter', () => {
    //public needs to be in uncompressed format
    expect(function () { service.createDIDDocument("3237327fhfdh23237"); }).toThrow(new Error("could not detect Public Key format"));
  });
  it('create Did Document correct', () => {
    let doc: DidDocument = service.createDIDDocument(publicKeyUncompressed);
    expect(doc).toBeTruthy();
    // JSON.stringify, because publicKey would lose \n
    expect(JSON.stringify(doc)).toContain("publicKeyJwk");
  });
  it('addProof wrong input parameters ', () => {
    //create DID document
    let doc: DidDocument = service.createDIDDocument(publicKeyUncompressed);
    expect(function () { service.addProof(doc, ""); }).toThrow(new Error("wrong input parameters"));
  });
  it('addProof wrong input parameters ', () => {
    //create DID document
    let doc: DidDocument = service.createDIDDocument(publicKeyUncompressed);
    expect(function () { service.addProof(null, null); }).toThrow(new Error("wrong input parameters"));
  });
  it('addProof proof is then available ', () => {
    //create DID document
    let doc: DidDocument = service.createDIDDocument(publicKeyUncompressed);
    const signatureValue = "0xd6b525106ef38260337396a07944dc6340bb99d3ebebe1962445fb343df6f5e131e92c013d3f406bef616dbb02abfaefb8f773dc0ef859f2a9d9b13d3e937ab11b";
    service.addProof(doc, signatureValue);
    expect(JSON.stringify(doc)).toContain("proof");
    expect(JSON.stringify(doc)).toContain("publicKeyJwk");
  });
});
