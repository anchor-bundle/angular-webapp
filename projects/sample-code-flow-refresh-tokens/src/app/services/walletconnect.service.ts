import { Injectable } from '@angular/core';
import WalletConnect from '@walletconnect/client';
import QRCodeModal from "@walletconnect/qrcode-modal";
import { Keccak } from 'sha3';
import { WalletNotConnectedError } from '../errors/WalletNotConnectedError';

@Injectable({
  providedIn: 'root'
})
export class ConnecetToWalletService {



  constructor() { }

  /*
  connect to a Wallet and returs the connection object
  */
  initCommunication(): Promise<WalletConnect> {

    let connector: WalletConnect;

    connector = new WalletConnect({
      bridge: "https://bridge.walletconnect.org", // Required
      qrcodeModal: QRCodeModal,
    });
    return new Promise(resolve => {

      // Check if connection is already established

      if (!connector.connected) {
        console.log("not already connected");

        // create new session
        connector.createSession();
      } else {
        console.log("already connected");
        resolve(connector);
      }

      // Subscribe to connection events^
      connector.on("connect", (error, payload) => {
        if (error) {
          throw error;
        }
        console.log("connected");
        console.log(payload);

        // Get provided accounts and chainId
        const { accounts, chainId } = payload.params[0];
        resolve(connector);
      });

      connector.on("session_update", (error, payload) => {
        if (error) {
          throw error;
        }
        console.log("session update");
        // Get updated accounts and chainId
        const { accounts, chainId } = payload.params[0];
      });

      connector.on("disconnect", (error, payload) => {
        console.log("disconnect");
        if (error) {
          console.log("error:");
          console.log(error);
          throw error;
        }
        console.log("disconnected");
        // Delete connector
      });

    })

  }
  /*
  signes a message with the private Key of the user
  parameter:
  message - a string that needs to be signed
  connector - a working connection to the users wallet
  returns:
  signed message string
  -1 if there is no connection to the user
  throws WalletNotConnectedError if wallet is not connected
  */
  async signMessage(message: string, connector: WalletConnect): Promise<string> {
    let signedMessage: string = "-2";
    if (!connector.connected) {
      console.log("walletconnect is not connected!");
      throw new WalletNotConnectedError();
    }
    console.log("start signing message");
    let address: string = connector.accounts[0];
    const hash = new Keccak(256);
    hash.update("\x19Ethereum Signed Message:\n" + (message.length) + message);
    const msgParams = [
      address,
      "0x" + hash.digest('hex'),
    ];
    // Sign personal message
    await connector.signMessage(msgParams)
      .then((result) => {
        // Returns signature.
        console.log("signes sucessfull:");
        signedMessage = result;
      })
      .catch((error) => {
        // Error returned when rejected
        console.log("signing failed:");
        console.error(error);
        signedMessage = "-2";
      });
    //check if some error occured:
    if (signedMessage == "-2" || !signedMessage) {
      throw new Error("some Error occured while signing Message");
    }
    return signedMessage;
  }

}
