import { TestBed } from '@angular/core/testing';
import { AuthenticateApi } from '../models/authenticate.model';
import { FilancoreApiService } from './filancore-api.service';
describe('FilancoreApiService', () => {
  let service: FilancoreApiService;
  let originalTimeout;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilancoreApiService);
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  afterEach(function () {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('authenticate should bei true if access token is already available', async () => {
    let authenticationDetails = service['authenticationDetails'] = new AuthenticateApi();
    authenticationDetails.accessToken = 'exampleToken1234';
    expect(await service.authenticate()).toBe(true);
    expect(service['authenticationDetails'].accessToken).toBe('exampleToken1234');
  });
  it('authenticate should bei true and access token getted from api', async () => {
    // mock getAccessToken function
    spyOn(service, "getAccessToken").and.callFake(async function () {
      let authenticationDetails = service['authenticationDetails'] = new AuthenticateApi();
      authenticationDetails.accessToken = 'exampleToken1234';
      authenticationDetails.tokenType = "Bearer";
    });

    expect(await service.authenticate()).toBe(true);
    expect(service['authenticationDetails'].accessToken).toBeTruthy();
  });

  it('authenticate should throw Error if getAccessToken was not working', async () => {
    // mock getAccessToken function
    spyOn(service, "getAccessToken").and.callFake(async function () {
      throw new Error('Error while getting access token');
    });
    await expectAsync(service.authenticate()).toBeRejectedWith(new Error('Error while getting access token'));
  });
  it('authenticate should throw Error if accesstoken is not available after api call', async () => {
    // mock getAccessToken function
    spyOn(service, "getAccessToken").and.callFake(async function () {
      let authenticationDetails = service['authenticationDetails'] = new AuthenticateApi();
      authenticationDetails.accessToken = undefined;
      authenticationDetails.tokenType = "Bearer";
    });
    await expectAsync(service.authenticate()).toBeRejectedWith(new Error('error while getting access token'));
  });
  it('authenticate should throw Error if accesstoken type is NOT Bearer', async () => {
    // mock getAccessToken function
    spyOn(service, "getAccessToken").and.callFake(async function () {
      let authenticationDetails = service['authenticationDetails'] = new AuthenticateApi();
      authenticationDetails.accessToken = undefined;
      authenticationDetails.tokenType = "NotBearer";
    });
    await expectAsync(service.authenticate()).toBeRejectedWith(new Error('error while getting access token'));
  });

  it('getAccessToken throw Error if credentials are wrong', async () => {
    // change raw variable in getAccessToken
    await expectAsync(service.getAccessToken()).toBeRejectedWith(new Error('Error while getting access token'));
  });
  it('deserialize correct', () => {
    let result = {
      tokenType: "Bearer",
      accessToken: "exampleToken123"
    }
    let authenticationDetails: AuthenticateApi = new AuthenticateApi().deserialize(result);
    expect(authenticationDetails.accessToken).toBeTruthy();
    expect(authenticationDetails.tokenType).toBeTruthy();

  });
  it('deserialize throw Error if deserialization is not possible', () => {
    let result = {
      wrongType: "Bearer",
      accessToken: "exampleToken123"
    }
    expect(function () { new AuthenticateApi().deserialize(result); }).toThrow(new Error("error while deserialize access token"));
  });
  /*  it('getAccessToken works', async () => {
     service.getAccessToken();
     let authenticationDetails = service['authenticationDetails'];
     console.log(authenticationDetails.accessToken);
     console.log(authenticationDetails.tokenType);
 
 
     expect(authenticationDetails.accessToken).toBeTruthy();
     expect(authenticationDetails.tokenType).toBeTruthy();
   }); */
  //TODO: test publishDidDoc
});
