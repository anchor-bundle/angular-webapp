import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthConfigModule } from '../auth-config.module';

import { UserNotLoggedinGuardService } from './user-not-loggedin-guard.service';

describe('UserNotLoggedinGuardService', () => {
  let service: UserNotLoggedinGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AuthConfigModule, RouterTestingModule],
    });
    service = TestBed.inject(UserNotLoggedinGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
