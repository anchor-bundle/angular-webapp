import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { OidcSecurityService } from 'angular-auth-oidc-client';

@Injectable({
  providedIn: 'root'
})
export class UserNotLoggedinGuardService implements CanActivate {

  constructor(public oidcSecurityService: OidcSecurityService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {

      this.oidcSecurityService.isAuthenticated().subscribe(isAuth => {

        if (isAuth) {
          resolve(true);
          return;
        } else {
          resolve(false);
          alert("sie müssen sich einloggen, um auf diese Seite zugreifen zu können");
          return;
        }


      });

    });

  }
}