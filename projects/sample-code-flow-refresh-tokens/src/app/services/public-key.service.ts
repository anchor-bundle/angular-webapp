import { Injectable } from '@angular/core';
import { ethers } from 'ethers';
var KeyEncoder = require('key-encoder');


@Injectable({
  providedIn: 'root'
})
export class PublicKeyService {

  constructor() { }

  /*
  getPublicKey
  this method calculates the public key(from a User) with a message and the signed version of this message
  parameters:
  message - plain text messagt, which the user will sign
  signedMessage - signed Version of the message. User signed it with his private Key
  options - Type of public key (pem,compressed,uncormpressed)
  retrun:
  public Key without 0x at beginn
  throws Error- if input parameter is invalid
  */
  getPublicKey(message: string, signedMessage: string, options: PublicKeyType): string {
    if (message && signedMessage) {
      const msgHash = ethers.utils.hashMessage(message);
      const msgHashBytes = ethers.utils.arrayify(msgHash);
      let publicKey = ethers.utils.recoverPublicKey(msgHashBytes, signedMessage);
      // 

      //check which keyformat is needed:
      switch (options) {
        case PublicKeyType.compressed:
          let publickeycomp: string = ethers.utils.computePublicKey(publicKey, true)
          return publickeycomp.slice(2, publicKey.length);
        case PublicKeyType.uncompressed:
          return publicKey.slice(2, publicKey.length);
        case PublicKeyType.pem:
          return this.convertPublicKeyToPEM(publicKey);
        default:
          throw new Error("error while getting public Key");
      }
    } else {
      throw new Error("invalid input Parameter");
    }
  }/*
  converts public key from uncompressed format into pem format
  used library: key-encoder
  parameters:
  public Key - uncompressed public key
  retrun value:
  public key in pem format
  */
  convertPublicKeyToPEM(publicKey: string): string {
    //check if the public key begins with 0x and delete it
    if (publicKey.substring(0, 2) === '0x') {
      publicKey = publicKey.substring(2, publicKey.length);
    }
    // check if public key is in the correct format:
    if (publicKey.length === 130 && publicKey.substring(0, 2) === '04') {
      let keyEncoder = new KeyEncoder.default('secp256k1')
      var publicKeyPEMExtracted = keyEncoder.encodePublic(publicKey, 'raw', 'pem');
      return publicKeyPEMExtracted;
    } else {
      throw Error('public key is in the wrong format');
    }

  }
  /*
  checks the format of a public Key and returns the format
  parameters:
  publicKey - publicKey which format needs to be checked (with or without 0x)
  returns:
  PublicKeyType - format of the public Key
  throws Error if format could not be detected or input parameters are missing/invalid
  */
  checkFormat(publicKey: string): PublicKeyType {
    if (publicKey) {
      //check if the public key begins with 0x and delete it
      if (publicKey.substring(0, 2) === '0x') {
        publicKey = publicKey.substring(2, publicKey.length);
      }
      switch (publicKey.substring(0, 2)) {
        case '04':
          return PublicKeyType.uncompressed;
        case '03':// if y is odd
          return PublicKeyType.compressed;
        case '02'://if y is even
          return PublicKeyType.compressed;
        default:
          if (publicKey.includes("BEGIN PUBLIC KEY")) {
            return PublicKeyType.pem
          } else {
            throw new Error("could not detect Public Key format");
          }
      }
    } else {
      throw Error("wrong input parameters");
    }

  }
}
export enum PublicKeyType {
  uncompressed,
  compressed,
  pem,
}