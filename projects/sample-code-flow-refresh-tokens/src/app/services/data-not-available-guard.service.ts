import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class DataNotAvailableGuardService implements CanActivate {

  constructor(private dataService: DataService, private router: Router) { }
  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    let claims;
    await this.dataService.claims.subscribe((result) => {
      claims = result;
    });
    if (claims) {
      return true;
    } else {
      this.router.navigate(['register']);
      alert("first fill in your data");
      return false;
    }
  }
}
