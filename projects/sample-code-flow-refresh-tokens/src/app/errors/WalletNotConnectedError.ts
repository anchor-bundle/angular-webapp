class WalletNotConnectedError extends Error {

    constructor() {
        super("Wallet is not connected");
    }
}
export {
    WalletNotConnectedError
};
