import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { EventTypes, OidcSecurityService, PublicEventsService } from 'angular-auth-oidc-client';
import { filter } from 'rxjs/operators';
import { AccountComponent } from './account/account.component';
import { AppComponent } from './app.component';
import { AuthConfigModule } from './auth-config.module';
import { HomeComponent } from './home/home.component';
import { ImportVcComponent } from './import-vc/import-vc.component';
import { LoginComponent } from './login/login.component';
import { OidcConfigComponent } from './oidc-config/oidc-config.component';
import { RegisterProcessComponent } from './register-Process/register-process.component';
import { RegisterComponent } from './register/register.component';
import { DataNotAvailableGuardService } from './services/data-not-available-guard.service';
import { LocalStorageManagementService } from './services/local-storage-management.service';
import { UserNotLoggedinGuardService } from './services/user-not-loggedin-guard.service';
import { VcCreatorService } from './services/vc-creator.service';
import { ConnecetToWalletService } from './services/walletconnect.service';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { AlertModule } from './_alert';
@NgModule({
  declarations: [AppComponent, HomeComponent, UnauthorizedComponent, RegisterComponent, RegisterProcessComponent, LoginComponent, ImportVcComponent, OidcConfigComponent, AccountComponent],
  imports: [
    BrowserModule,
    FormsModule,
    AlertModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'forbidden', component: UnauthorizedComponent },
      { path: 'unauthorized', component: UnauthorizedComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'register/process', component: RegisterProcessComponent, canActivate: [DataNotAvailableGuardService] },
      { path: 'login', component: LoginComponent },
      { path: 'importVc', component: ImportVcComponent },
      { path: 'oidcConfig', component: OidcConfigComponent },
      { path: 'myaccount', component: AccountComponent, canActivate: [UserNotLoggedinGuardService] },
    ]),
    AuthConfigModule,
  ],
  providers: [VcCreatorService, OidcSecurityService, DataNotAvailableGuardService, LocalStorageManagementService, ConnecetToWalletService],
  bootstrap: [AppComponent],
  exports: [MatCheckboxModule]
})
export class AppModule {
  constructor(private readonly eventService: PublicEventsService) {
    this.eventService
      .registerForEvents()
      .pipe(filter((notification) => notification.type === EventTypes.ConfigLoaded))
      .subscribe((config) => {
        console.log('ConfigLoaded', config);
      });
  }
}
