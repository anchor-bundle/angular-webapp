import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthConfigModule } from '../auth-config.module';

import { OidcConfigComponent } from './oidc-config.component';

describe('OidcConfigComponent', () => {
  let component: OidcConfigComponent;
  let fixture: ComponentFixture<OidcConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AuthConfigModule, RouterTestingModule],
      declarations: [OidcConfigComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(OidcConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
