import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OidcSecurityService, OpenIdConfiguration } from 'angular-auth-oidc-client';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-oidc-config',
  templateUrl: './oidc-config.component.html',
  styleUrls: ['./oidc-config.component.css']
})
export class OidcConfigComponent implements OnInit {
  configuration$: Observable<OpenIdConfiguration>;
  constructor(public oidcSecurityService: OidcSecurityService, private router: Router) { }

  ngOnInit(): void {
    this.configuration$ = this.oidcSecurityService.getConfiguration();
  }


  toHome() {
    this.router.navigate(['home']);

  }
}
