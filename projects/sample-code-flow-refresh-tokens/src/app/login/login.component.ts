import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import WalletConnect from '@walletconnect/client';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import canonicalize from 'canonicalize';
import { Did } from 'projects/sample-code-flow-refresh-tokens/libraries/did_nodejs/core/ssi/Did.js';
import { Proof } from "../../../libraries/did_nodejs/core/ssi/Proof.js";
import { PublicKeyReference } from "../../../libraries/did_nodejs/core/ssi/PublicKeyReference.js";
import { Vc } from '../../../libraries/did_nodejs/core/ssi/Vc.js';
import { WalletNotConnectedError } from '../errors/WalletNotConnectedError';
import { VP } from '../helpClasses/VP';
import { LocalStorageManagementService } from '../services/local-storage-management.service';
import { PublicKeyService, PublicKeyType } from '../services/public-key.service';
import { ConnecetToWalletService } from '../services/walletconnect.service';
import { AlertService } from '../_alert';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //checkboxs in the html
  box1: boolean = false;
  box2: boolean = false;
  box3: boolean = false;
  box4: boolean = false;
  box5: boolean = false;
  box6: boolean = false;

  //indeterminate Boxes in the html
  indeterminateBoxes = [false, false, false, false, false, false];
  connector: WalletConnect;
  constructor(private publicKeyService: PublicKeyService, private alertService: AlertService, private localStorage: LocalStorageManagementService, private router: Router, private walletconnect: ConnecetToWalletService, public oidcSecurityService: OidcSecurityService) { }

  ngOnInit(): void {
    this.startLoginProcess();
  }
  async startLoginProcess() {
    try {
      console.log("start login process");

      // STEP 1: get VC from Browser Storage
      let vc: Vc = this.getVCFromStorage();
      this.box1 = true;
      console.log("STEP 1 check");

      // STEP 2: add Timestamp to VC
      let vpWithTimestamp: VP = this.addTimeStamp(vc);
      this.box2 = true;
      console.log("STEP 2 check");

      // STEP 3: let the user sign the VP
      let signitureVP: string = await this.letUserSignVP(vpWithTimestamp);
      console.log("signatureVP:");
      console.log(signitureVP);


      console.log("vpwithTimestamp");
      console.log(canonicalize(vpWithTimestamp.toJSON()));
      this.box3 = true;
      console.log("STEP 3 check");
      //STEP 4: add proof to VP
      //get did from signer, this is needed to generate proof
      let publicKeySigner = this.publicKeyService.getPublicKey(canonicalize(vpWithTimestamp.toJSON()), signitureVP, PublicKeyType.uncompressed);
      let didSigner = Did.fromPublicKey(publicKeySigner)

      this.addProofToVP(vpWithTimestamp, signitureVP, didSigner);
      this.box4 = true;
      console.log("STEP 4 check");

      //STEP 5: save VP in Cookies
      this.setCookie("VP", JSON.stringify(vpWithTimestamp.toJSON()), 3, '/realms/iota');
      this.box5 = true;
      console.log("STEP 5 check");
      //STEP 6: start OIDC request
      this.oidcSecurityService.authorize();
      console.log("STEP 6 check");
      this.box6 = true;


    } catch (error) {
      console.log("error:");
      console.log(error);
      this.setNextCheckboxfailed();


    }


  }
  /*
   this function is called if an error occures
   this function checks at wich step the process failed and highlight the depending checkbox
   */
  setNextCheckboxfailed() {
    let checkboxes = [this.box1, this.box2, this.box3, this.box4, this.box5, this.box6];
    if (this.box1 == false) {
      this.indeterminateBoxes[0] = true;
    } else {
      for (let index = 0; index < checkboxes.length; index++) {
        if (checkboxes[index] === true && checkboxes[index + 1] === false) {
          this.indeterminateBoxes[index + 1] = true;
        }
      }
    }
  }

  /*
  add Proof to the VP
  parameters:
  vp - Vp object to which the proof is added as attribute
  signitureVP- signature (from user signed string version of VP)
  didSubject - DID from Subject(extracted from VC(local Storage)). Is needed because it is a field in the proof
  returns:
  nothing, vp object is changed(added proof field)
  throws Error if inuput parameters are null or undefined
  */
  addProofToVP(vp: VP, signitureVP: string, didSubject: string) {
    if (vp && signitureVP && didSubject) {
      //> Check if there is a proof already!
      if (vp._data.proof !== undefined) {
        throw new Error('proof already exists!');
      };
      const creatorPKR = new PublicKeyReference(didSubject, 'key-1');
      vp._data.proof = new Proof(signitureVP, creatorPKR, undefined, undefined, "assertionMethod");
    } else {
      throw new Error("missing input parameters");
    }
  }

  /*
  let the user sign a the VP
  parameters:
  VP - VP object that needs to be signed
  return:
  signed VP(signed from user)
  errors are catched in the startLoginProcess method
  */
  async letUserSignVP(vp: VP): Promise<string> {
    if (vp) {
      // connect to Wallet from user
      this.connector = await this.walletconnect.initCommunication();
      if (this.connector.connected) {
        let signedData: string;
        const message = canonicalize(vp.toJSON());
        this.alertService.info("Please open your wallet and sign the message");
        await this.walletconnect.signMessage(message, this.connector).then((signedMessage) => {
          signedData = signedMessage;
        });
        return signedData;
      } else {
        console.log("not connected");
        throw new WalletNotConnectedError();
      }
    } else {
      throw Error('input parameter is missing');
    }

  }
  /*
  adds timestamp to vc by creating a new VP object which contains the VC and a timestamp
  parameters:
  vc - verifiable credentials, read from browser storage
  return
  vp: verifiable presentation with timestamp
  */

  addTimeStamp(vc: Vc): VP {
    let vp: VP = new VP(vc);
    return vp;

  }
  /*
  read VC from local strorage and parse it back to an object of type VC
  if no vc is saved in the browser, the user is redirected to the register form
  return:
  vc - vc object

  */
  getVCFromStorage(): Vc {
    let vc: string = this.localStorage.getVC();
    if (vc === null || !vc) {
      //vc is not in localstorage
      alert("no VC available in your Browser storage. You need to register");
      this.router.navigate(['register']);
      throw new Error("vc is not available from Browserstorage");
    } else {
      // convert vc string -> vs JSON -> vc object
      vc = Vc.fromJSON(JSON.parse(vc));
      return vc;
    }
  }
  /*
  this method sets a cookie in the browser
  the parameters are the properies of this cookie
  */
  setCookie(name: string, value: string, expireMins: number, path: string = '') {
    //cookies can not handle "," so the json vp need to be encoded
    value = encodeURIComponent(value);
    let d: Date = new Date();
    d.setTime(d.getTime() + expireMins * 60 * 1000);
    let expires: string = `expires=${d.toUTCString()}`;
    let cpath: string = path ? `; path=${path}` : '';
    document.cookie = `${name}=${value}; ${expires}${cpath}`;
  }

  /*
 this is called if the user is already connected and want to connect to a different wallet
 */
  async connectToNewWallet() {
    console.log("connect to new Wallet");
    await this.connector.killSession();// diconnect from wallet
    this.resetCheckboxes();
    this.startLoginProcess();

  }
  /*
    unchecks every checkbox
    */
  resetCheckboxes() {
    this.box1 = false;
    this.box2 = false;
    this.box3 = false;
    this.box4 = false;
    this.box5 = false;
    this.box6 = false;
    //indeterminate Boxes in the html
    for (let index = 0; index < this.indeterminateBoxes.length; index++) {
      this.indeterminateBoxes[index] = false;

    }

  }
}
