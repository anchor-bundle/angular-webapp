import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RouterTestingModule } from '@angular/router/testing';
import { Vc } from '../../../libraries/did_nodejs/core/ssi/Vc.js';
import { AuthConfigModule } from '../auth-config.module';
import { VP } from '../helpClasses/VP';
import { VcCreatorService } from '../services/vc-creator.service';
import { AlertModule } from '../_alert';
import { LoginComponent } from './login.component';
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let vcCreatorService: VcCreatorService;
  let publicKeySubject;
  let claims;
  beforeEach(async () => {
    publicKeySubject = '046d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c';

    claims = [
      { "claimName": "firstName", "claimContent": "max" },
      { "claimName": "lastName", "claimContent": "mustermann" },
      { "claimName": "nickname", "claimContent": "m.mustermann" },
      { "claimName": "town", "claimContent": "berlin" },
      { "claimName": "email", "claimContent": "m.muster@gmail.com" }
    ];
    LoginComponent.prototype.ngOnInit = () => { };
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, AuthConfigModule, AlertModule, MatCheckboxModule, FormsModule],
      declarations: [LoginComponent],
    })
      .compileComponents();
    vcCreatorService = TestBed.inject(VcCreatorService);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('addProofToVP - wrong input parameters', () => {
    // first create VC
    let vc: Vc = vcCreatorService.generateVC(null, publicKeySubject, claims);
    // create VP
    let vp: VP = new VP(vc);
    expect(function () { component.addProofToVP(vp, null, "testDIDSubject"); }).toThrow(new Error("missing input parameters"));
  });
  it('addProofToVP-  wrong input parameters', () => {
    // first create VC
    let vc: Vc = vcCreatorService.generateVC(null, publicKeySubject, claims);
    // create VP
    let vp: VP = new VP(vc);
    expect(function () { component.addProofToVP(vp, "exampleSignature", ""); }).toThrow(new Error("missing input parameters"));
  });
  it('addProofToVP proof- create VP correct', () => {
    // first create VC
    let vc: Vc = vcCreatorService.generateVC(null, publicKeySubject, claims);
    // create VP
    let vp: VP = new VP(vc);
    expect(vp).toBeTruthy();
  });
});
