import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OidcClientNotification, OidcSecurityService, UserDataResult } from 'angular-auth-oidc-client';
import { Observable } from 'rxjs';
import { DataService } from '../services/data.service';
import { ConnecetToWalletService } from '../services/walletconnect.service';
import { AlertService } from '../_alert';
@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
})
export class HomeComponent implements OnInit {
  userDataChanged$: Observable<OidcClientNotification<any>>;
  userData$: Observable<UserDataResult>;
  isAuthenticated = false;
  neededDataAvailable = false;
  constructor(public oidcSecurityService: OidcSecurityService, private router: Router, private alertService: AlertService, private walletconnect: ConnecetToWalletService, private dataservice: DataService) { }

  ngOnInit() {

    this.userData$ = this.oidcSecurityService.userData$;
    this.getsaveUserData();


    // save oidcService for login Component(to get the answer from the oidc call)
    //this.dataService.changeOIDCSecurityService(this.oidcSecurityService);
  }
  /**
   * check if user in logged in and userdata is available from Identity provider
   * if yes: save userdata for other component
   */
  getsaveUserData() {
    this.oidcSecurityService.isAuthenticated$.subscribe(({ isAuthenticated }) => {
      this.isAuthenticated = isAuthenticated;

      if (this.isAuthenticated) {
        this.oidcSecurityService.userData$.subscribe(userdata => {
          // validate if every needed property is available
          // example data needed: town and email
          if (userdata.userData && userdata.userData.email && userdata.userData.town) {
            this.dataservice.changeUserdata(userdata);
            console.log("saved userdata!!");
            this.neededDataAvailable = true;
            console.log("all needed data available");

          } else {
            this.dataservice.changeUserdata(userdata);
            console.log("saved userdata!!");
            console.log("data missing");
            this.neededDataAvailable = false;
          }
        });
      }
      console.warn('authenticated: ', isAuthenticated);
    });
  }

  login() {
    // redirect to login component
    this.router.navigate(['login']);
  }

  register() {
    //open new Page with register formular
    this.router.navigate(['register']);
  }

  refreshSession() {
    this.oidcSecurityService.forceRefreshSession().subscribe((result) => console.log(result));
  }

  logout() {
    this.oidcSecurityService.logoff();
  }

  logoffAndRevokeTokens() {
    this.oidcSecurityService.logoffAndRevokeTokens().subscribe((result) => console.log(result));
  }

  revokeRefreshToken() {
    this.oidcSecurityService.revokeRefreshToken().subscribe((result) => console.log(result));
  }

  revokeAccessToken() {
    this.oidcSecurityService.revokeAccessToken().subscribe((result) => console.log(result));
  }

  importVC() {
    //redirect to ImportVc Component
    this.router.navigate(['importVc']);
  }
  oidcConfig() {
    this.router.navigate(['oidcConfig']);
  }
  myaccount() {
    this.router.navigate(['myaccount']);
  }

}
