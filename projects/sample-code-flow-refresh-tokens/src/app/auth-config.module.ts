import { NgModule } from '@angular/core';
import { AuthModule, LogLevel } from 'angular-auth-oidc-client';

@NgModule({
  imports: [
    AuthModule.forRoot({
      config: {
        authority: 'http://localhost:8080/realms/iota',
        redirectUrl: window.location.origin,
        postLogoutRedirectUri: window.location.origin,
        clientId: 'angular-with-oidc',
        scope: 'openid profile email offline_access town',
        responseType: 'code',
        // autoUserInfo: true,
        silentRenew: true,
        useRefreshToken: true,
        logLevel: LogLevel.Debug,
        ignoreNonceAfterRefresh: true,
        renewUserInfoAfterTokenRenew: false,
        tokenRefreshInSeconds: 30
      },
    }),
  ],
  exports: [AuthModule],
})
export class AuthConfigModule { }
