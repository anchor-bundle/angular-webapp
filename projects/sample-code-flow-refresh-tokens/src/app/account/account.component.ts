import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  userData;
  constructor(private dataservice: DataService, private alertService: AlertService, private router: Router) { }

  ngOnInit(): void {
    this.getUserData();
  }
  /**
   * get Userdate from dataservice
   * the userdata is from the oidc response and was saved by the Homecomponent
   */
  getUserData() {
    this.dataservice.userdata.subscribe(userdata => {
      if (userdata && userdata.userData) {
        this.userData = userdata.userData;

      } else {
        this.alertService.error("Error while getting userdata");
      }
    });

  }
  tohome() {
    this.router.navigate(['home']);
  }


}

