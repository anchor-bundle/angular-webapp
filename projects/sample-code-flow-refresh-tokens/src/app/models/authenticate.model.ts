import { Deserializable } from "./deserializable.model";

export class AuthenticateApi implements Deserializable {

    tokenType: string;
    accessToken: string;

    deserialize(input: any) {
        Object.assign(this, input);
        // check if deserialization was correct
        if (this.tokenType && this.accessToken) {
            return this;
        } else {
            throw new Error("error while deserialize access token");
        }

    }

}
