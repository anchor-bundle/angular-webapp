# Anchor

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](./CODE_OF_CONDUCT.md)
[![pipeline status](https://gitlab.com/anchor-bundle/angular-webapp/badges/main/pipeline.svg)](https://gitlab.com/anchor-bundle/angular-webapp/keycloak-extension/commits/main)

This project is a prototype application that supports 'Login with IOTA' as a single-sign on(SSO) using self-sovereign identity(SSI). The project was undertaken as part of an internal bacherlor thesis work performed by [Tim Altmann](https://gitlab.com/TimAltmann) at the ETO GRUPPE TECHNOLOGIES GmbH. Following the success of the project and internal discussion, it was decided to bring the project to community and build solutions together with everyone.

## Features

- Create DID and VC for User
- Publish DID on Tangle for User
- Import existing VC from User
- OpenId connect request with VC to Login with IOTA
- Resolves DID from tangle and verify the signature of the VC
- User is able to share only the data he wants to share with the client application

## Components

- Angular frontend (Port: 4204): this litte project is an example website with the functionality to create DIDs/VCs and start an OpenId connect login request with it.
- [Keycloak extension:](https://gitlab.com/anchor-bundle/keycloak-extension) (Port: 8080): extends Keycloak with the function to login with a DID.
- [DID connect](https://gitlab.com/anchor-bundle/did-connect) (Port: 5555): Nodejs endpoint that is able to resolve and publish DID Documents from/to the Tangle (Devnet).

>CAUTION: this is only a quick workaround. Later the resolving/publishing and verifying of the signature is performed by an API, provided by the [IOTA Identity](https://github.com/iotaledger/identity.rs) library

## Getting started

### Install

Clone this project with the --recurse-submodules flag

#### Requirements

- Docker (tested with version: 20.10.17)

To start the application:

    double click build&start.bat

or manually:

    cd ./keycloak-extension/
    ./gradlew jar
    cd ../
    docker-compose up

Keycloak, Angular frontend and the Nodejs endpoint are each running inside their own docker container. They are all started with one docker-compose file.
>Caution: you have to diable CORS to run this project successful

### Usage

The angular application is able to create a DID and VC for the user (if the user is not already having one). Then the angular application is starting an OpenId connect request to the Keycloak server. Then Keycloak can verify the signature of the VC and grand or decline access to the application.

### Wallet

The user needs to sign the VC and DID document with his wallet. The communication with the wallet is implemented with [Walletconnect](https://walletconnect.com). It seems like some wallets are not using the secp256k1 curve to sign messages. These wallets will not work.

Tested wallets are:

- MetaMask
- Trust Wallet

### Architecture and Design

The Software Architecture and Requirements that were important for the development can be found [here](./documentation/README.md)

### Development

This project was developed in a docker dev container. In this container the development environment with all dependencies is already installed.

To use the dev container you need:

- Visual Studio Code
- Visual Studio Code Extension: Remote - Containers

### Tests

The testframework used in this project is jasmine karma. To run the tests run the following command in the terminal:

    npm run test:ci

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
