# Keycloak Extension
# Development
the software was developed based on this requirements:

## Requirements

| User Story | Description                                                                                                                                                                   |
|------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| U-01       | As a user, I would like to be able to log in to a website with my DID and VC so that I don't have to create an extra account.                                               |
| U-02       | As a user, I would like to be able to log in with my own existing DID so that I don't have to create a new DID                                                   |
| U-03       | As a user, I would like to be able to use my existing wallet for the login process so that I don't have to install a new wallet                                        |
| U-04       | As a user, I would like to be able to log in to my cell phone and my PC with my DID so that I am not tied to a device                                          |
| U-05       | As a user, I would like to be asked every time my data is released so that I remain in control of my data                                        |
| U-06       | As a user, I would like to be able to have a DID and VC created for me if I don't already have one, so that I can log in to many other sites |
| U-07       | As a user, I want to see exactly what data I share with the website so that I can keep control of my data                                                |
| U-08       | As a user, I would like to be able to refuse a data exchange if, in my opinion, the website wants too much data from me                               |
| U-09       | As a provider, I would like to be able to set role-based approval for each DID to control authorization in my system                   |
| U-10       | As a user, I would like to be able to revoke a website's access to my data in order to have control over my data at all times                    |
| U-11       | As a provider, I would like the VC/VP to be checked for signatures so that no attacker with forged data can access the system.                           |
| U-12       | As a user, I don't want to be limited to one wallet so that I don't have to create a new wallet, but can use the one I already have.                              |
### Constraints

| Constraint | Description                                                                                                                                                        |
|------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| C-01       | ETO wants to use open standards so that they appear trustworthy to their customers                                                                             |
| C-02       | The system must be GDPR compliant, which is automatically given if the DID standard is implemented correctly                                                       |
| C-03       | The Identity Provider should be Keycloak, since ETO is in the process of realizing several other projects with a Keycloak instance                              |
| C-04       | The project must be completed by August 31st                                                                                                                 |
| C-05       | This project follows an open source strategy                                                                                                         |
| C-06       | Check for licenses of libraries used and act accordingly (e.g. publish code based on license)                                      |
| C-07       | The distributed storage used in this project is said to be Iota's 'Tangle' due to several advantages over other DLTs |

### quality attributes

| Attribute                   | Description                                                                                                                        |
|----------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| Devtime quality attribute |                                                                                                                                     |
| QA-01                      | The identity provider must be extensible to implement the desired functionality                                     |
| Runtime quality attribute |                                                                                                                                     |
| QA-02                      | The user's private key must be kept as safe as possible at all times so that the user is not vulnerable                 |
| QA-03                      | The Identity Provider must be scalable to run stabe in production mode, regardless of the number of users             |
| QA-04                      | The system should be usable for users who have no idea about the cryptography and the distributed ledger under the hood |
| QA-05                      | The login should work reliably                                                                                            |
| QA-06                      | The DLT used should be easily scalable (carry out many transactions quickly)                                               |

## Software Architecture
Based on the requirements a software architecture was designed. The software architecture is divided in three diagrams which are arranged from abstract to concrete:
## System context diagram
<img src="images/System context diagram.png" alt="System context diagram" width="800"/>

## Container diagram
<img src="images/Container Diagram.png" alt="Container diagram" width="800"/>

## Component diagram
<img src="images/componentDiagram.png" alt="component diagram" width="800"/>

