# Introduction

## Welcome to Anchor - A SSO-SSI Login solution

**Anchor** is a frontend browser based application which combines the single-sign on(SSO) via Oauth2.0 based Open ID Connect(OIDC) with Decentralized Identifiers(DID) and Verifiable Credentials (VC) standard. This effectively means that with Anchor it is possible to add single sign-on based on-boarding for your platform, which allows users to use their existing wallets to create self-sovereign identities. The plugin can be used by your identity provider to forward you the needed user data, where the user can decide every time during login, which data they would like to share with you.

Below are some links to the standards used and referred during the development of this project.

1. [Decentralized Identifiers](https://www.w3.org/TR/did-core/)
2. [Verifiable Credentials/ Verifiable Presenations](https://www.w3.org/TR/vc-data-model/)
3. [Oauth 2.0 RFCs](https://oauth.net/specs/ )
4. [Open ID Connect(OIDC) Specification](https://openid.net/specs/openid-connect-core-1_0.html)
5. [OIDC Self-issued Open ID Provider](https://openid.net/specs/openid-connect-self-issued-v2-1_0.html)
6. [OpenID for Verifiable Presentations](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html)

The specification for [OpenID for Verifiable Presentations](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html) was released during the later stages of the work. Hence, the current implementation of Anchor and the Key Cloak plugin does not confirm entirely to the specification. The VP is exchanged with the Open ID provider using the cookies method for now. An [issue](https://gitlab.com/anchor-bundle/angular-webapp/-/issues/22) already exists.

## We want to build with you

We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

We are looking forward for all kinds of contributions. From translations and UI/UX improvements, to documentation and new identity provider plug-ins for a login based on self-sovereign identity.

We Use [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html), So all code changes happens through Merge Requests.

## Any contributions you make will be under the AGPLv3 Software License

In short, when you submit code changes, your submissions are understood to be under the same [AGPLv3](./LICENSE) that covers the project. Feel free to contact the maintainers if that's a concern.

## Report bugs using GitLab's [issues](https://gitlab.com/anchor-bundle/angular-webapp/-/issues)

We use GitLab issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/anchor-bundle/angular-webapp/-/issues/new); it's that easy!

## Write bug reports with detail, background, and sample code

**Great Bug Reports** tend to have:

- A quick summary and/or background
- Steps to reproduce
  - Be specific!
  - Give sample code if you can.
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)
- Logs, if any
- Environment, if relevant

We *love* thorough bug reports.

## Community

You can reach out to us via GitLab directly. We are currently setting up our Discord, where you would be able to communicate more freely with us.

## Code of Conduct

See [CODE OF CONDUCT](./CODE_OF_CONDUCT.md)
